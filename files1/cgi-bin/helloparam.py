import os
from urllib.parse import parse_qs
import fileinput

name = parse_qs(os.environ['QUERY_STRING'])['name'][0]

print("Content-type: text/html\r\n\r\n");
print("Hello from Python, mister " + name);
