/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Response.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/27 10:30:17 by alouis            #+#    #+#             */
/*   Updated: 2021/09/29 17:39:53 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RESPONSE_HPP
#define RESPONSE_HPP

#include <iostream>
#include <string>
#include <ctime>
#include <fstream>
#include <vector>
#include <fstream>
#include <iomanip>
#include <sstream>

#include <dirent.h>
#include "RequestData.hpp"

class Response
{
    class NotCompliantException : public std::exception
    {
        virtual const char *what() const throw()
        {
            return "\033[1;33mNot HTTP/1.1 compliant\033[0m";
        }
    };

public:
    Response();
    Response(const int newStatus);
    ~Response();

    void    clear();

    ssize_t getResponseSize() const; // return 0 if whole response wasn't created with createResponse()

    void setStatusCode(int newStatusCode);
    void addToBody(std::vector<unsigned char> buff);
    void addToBody(std::string buff);
    void addToBodyAsChunk(std::vector<unsigned char> body);
	void addExtraHeader(std::string str);
	void addExtraHeader(std::vector<unsigned char> buff);
    void setContentLenght(ssize_t lenght);
    void setContentType(std::string mediaType);
    void setTransferEncoding(std::string tEncoding);
    void setLocation(std::string location);
	size_t getBodySize() const;
	std::vector<unsigned char> createResponse();
    std::vector<unsigned char>	createChunkBlock(std::vector<unsigned char> content);
	std::string formatDirListing(RequestData const& req, std::string const& initialUri);

private:
    Response(const Response &src);
    Response &operator=(const Response &src);

    std::string const createStatusMsg(int statusCode);
    void loadErrorPage(int statusCode);

    int _statusCode;
	std::vector<unsigned char> _body;
	std::vector<unsigned char> _extraHeader;
    std::string _statusLine;
    std::string _contentType;
    std::string _contentLength;
    std::string _date;
    std::string _transferEncoding;
    std::string _location;
    ssize_t _size;
    std::ifstream _file;
};

std::ostream &operator<<(std::ostream &os, Response const &rh);

#endif
