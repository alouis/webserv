/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RewriteOptions.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/12/17 19:33:35 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 19:35:09 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef REWRITEOPTIONS_HPP
#define REWRITEOPTIONS_HPP

#include <string>

typedef struct rewriteOption
{
	std::string from;
	std::string to;

} rewriteOption;

#endif
