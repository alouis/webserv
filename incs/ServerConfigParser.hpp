/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ServerConfigParser.hpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/26 11:52:08 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 20:41:49 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVERCONFIGPARSER_HPP
#define SERVERCONFIGPARSER_HPP
#include <iostream>
#include "ServerConfig.hpp"
#include "Routes.hpp"
#include <string>
#include <vector>
#include <fstream>
#include <sstream>

class ServerConfig;
class Routes;

class ServerConfigParser
{
public:
	class InvalidConfigPathException : public std::exception
	{
		virtual const char *what() const throw()
		{
			return "Invalid config path";
		}
	};

	class InvalidConfigFileException : public std::exception
	{
		virtual const char *what() const throw()
		{
			return "Invalid config file";
		}
	};

	ServerConfigParser(); //For mocked data
	ServerConfigParser(std::string configPath);
	~ServerConfigParser();
	ServerConfigParser(ServerConfigParser const &);
	ServerConfigParser &operator=(ServerConfigParser const &);

	std::vector<ServerConfig> buildServerConfigs();
	ServerConfig getDefaultConfig();

private:
	std::string _configPath;
	std::ifstream _src;
	std::vector<ServerConfig> buildMockData() const;
	std::vector<std::string> extractAttribute(std::string const& content, std::string const& attributeName) const;
	ServerConfig extractConfig(std::string const& content) const;
	void extractListen(std::string const& content, ServerConfig &conf) const;
	void extractServerNames(std::string const& content, ServerConfig &conf) const;
	void extractRoot(std::string const& content, ServerConfig &conf) const;
	void extractClientMaxBodySize(std::string const& content, ServerConfig &conf) const;
	void extractDefaultErrorPage(std::string const& content, ServerConfig &conf) const;
	void extractRoutes(std::string const& content, ServerConfig &conf) const;
	void extractRoutePath(std::string const &content, Routes &route) const;
	void extractRouteHTTPMethod(std::string const &content, Routes &route) const;
	void extractRouteHTTPRedirection(std::string const &content, Routes &route) const;
	void extractRouteRoot(std::string const &content, Routes &route) const;
	void extractRouteDefaultFileIfDirectoryRequest(std::string const &content, Routes &route) const;
	void extractRouteCGIExtension(std::string const &content, Routes &route) const;
	void extractRouteFileUploadDirectory(std::string const &content, Routes &route) const;
	void extractRouteErrorPage(std::string const &content, Routes &route) const;
	void extractAutoindex(std::string const &content, Routes &route) const;


	void readContent();
	std::vector<std::string> extractContents();
	std::vector<std::string> extractRoutesContent(std::string const &content) const;
	void checkConfigs(std::vector<ServerConfig> &configs);
};

std::ostream &operator<<(std::ostream &stream, ServerConfigParser const &cl);

#endif
