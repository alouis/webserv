/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RequestData.hpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 18:40:07 by clucien           #+#    #+#             */
/*   Updated: 2021/12/17 22:15:08 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#pragma once

#include <string>
#include <map>
#include <vector>
#include <list>
#include <iostream>
#include <stdlib.h>
#include "Routes.hpp"

/**
 * Post type
 */
#define MULTIPART 1
#define URLENCODED 2

/**
 * MULTIPART Parsing state
 */
#define DATA_CROPPED 1
#define BEGIN_OF_DATA_CROPPED 2
#define END_OF_DATA_CROPPED 3
#define DATA_COMPLETE 4

#define CPRINTL(str) std::cerr << str << std::endl; // debug printing

class Routes;

typedef std::multimap< std::string, std::string >	double_string_multimap;
typedef std::map< std::string, std::string >		double_string_map;
typedef std::pair< std::string, std::string >		double_string_pair;
typedef std::vector< unsigned char >				uchar_vector;

typedef struct s_postElement
{
	std::string name;
	uchar_vector data;
} t_postElement;

// Note: Why can't we put the variable in public as we need to set and get them ?
// Is getUriForm usefull ?
class RequestData
{
public:
	RequestData(void);
	// RequestData( std::string method, std::string requestUri, std::string httpVersion, double_string_multimap headers, std::string messageBody );
	RequestData(const RequestData &other);
	~RequestData(void);

	RequestData &operator=(const RequestData &other);

	void clear(void);
	void clearBody(void);
	int parseRequest(uchar_vector requestRaw);

private:
	int parseHead(uchar_vector *requestRaw);
	bool checkIfRequestIsChunked(void);
	void handlePostMethod(void);
	int checkHead(void);
	void parsePostMultipart(uchar_vector body);
	void parsePostUrlencoded(uchar_vector body);
	int parseBody( uchar_vector messageBody );
	int checkBody(void);
	int parseChunkedBody(uchar_vector *requestRaw);
	std::string const &headersSecureGet(std::string const key) const;
	/* Parse utils */
	void prepareRequest(uchar_vector *requestRaw);
	size_t ucharFind(uchar_vector const &vec, unsigned char const &charac, size_t from = 0);
	size_t ucharFind(uchar_vector const &vec, std::string const &str, size_t from = 0);
	std::string const ucharSubstr(uchar_vector::const_iterator begin, uchar_vector::const_iterator end) const;

	public:
		uchar_vector const				&getRequestRaw( void ) const;
		std::string	const				&getMethod( void ) const;
		std::string	const				&getRequestUri( void ) const;
		std::string const				getUriPath( void ) const;
		std::string const				getUriFile( void ) const;
		std::string	const				&getHttpVersion( void ) const;
		double_string_multimap			&getHeaders( void );
		uchar_vector const				&getMessageBody( void ) const;
		std::string	const				getStringMessageBody( void ) const;
		bool							getIsChunked( void ) const;
		bool							getIsCropped( void ) const;
		int								getStatusCode( void ) const;
		unsigned long					getBodyLength( void ) const;
		unsigned long					getContentLength( void ) const;
		std::string	const				&getContentType( void ) const;
		std::pair<double_string_multimap::const_iterator,double_string_multimap::const_iterator>
										getTransferEncoding( void ) const;
		int								getExpect( void ) const;
		std::string	const				&getHost( void ) const;
		std::vector<t_postElement>		&getPostElements( void );
		unsigned long					getTotalBytesRead( void ) const;
		int								getPostType( void ) const;
		bool							isUriChecked(void) const;
		Routes const&					getSelectedRoute() const;
		bool							getDirListing() const;
		bool							arePermissionsChecked(void) const;
		void							clearLastDataPostElem( void );
		void							clearTotalBytesRead( void );

		void							setUriPath( std::string newPath ); // reference ?
		void							setUriFile(std::string const& newFile);
		void							setIsChunked( bool value );
		void							setIsCropped( bool value );
		void							setStatusCode( int statusCode );
		void							setUriChecked(bool checked);
		void							setSelectedRoute(Routes const& route);
		void							setPermissionsChecked(bool checked);
		void							addToBytesRead( unsigned long bytesRead );
		void							setExpect(int value);
		void							setDirListing(bool value);

		void printRequest(void);

	private:
		/* Request elements */
		uchar_vector					_requestRaw;
		std::string						_method;
		std::string						_requestUri;
		std::string						_httpVersion;
		double_string_multimap			_headers;
		uchar_vector					_messageBody;
		/* Request info */
		bool							_isChunked;
		bool							_isFirstChunk;
		bool							_isCropped;
		int								_multipartState;
		int								_postType;
		unsigned long					_bodyLength;
		int								_statusCode;
		int								_erasedR;
		int								_headerLines;
		bool							_uriChecked;
		Routes 							_selectedRoute;
		bool							_permissionsChecked;
		bool							_dirListing;
		/* Post specs */
		std::string						_boundary;
 		std::vector< t_postElement >	_postElements;
		/* Request datas */
		unsigned long					_totalBytesRead;
};
