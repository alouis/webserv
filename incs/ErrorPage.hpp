/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   errorPage.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 21:51:48 by mroux             #+#    #+#             */
/*   Updated: 2021/11/25 00:37:56 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERRORPAGE_HPP
#define ERRORPAGE_HPP
#include <vector>
#include <string>
#include <fstream>

typedef struct errorPage
{
	std::vector<int> codes;
	std::string path;
} errorPage;

std::ostream &operator<<(std::ostream &stream, errorPage const &cl);

#endif
