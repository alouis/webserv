/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ServersMonitor.hpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 16:53:36 by alouis            #+#    #+#             */
/*   Updated: 2021/12/02 12:02:27 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVERSMONITOR_HPP
#define SERVERSMONITOR_HPP

#include <iostream>
#include <stdio.h>
#include <string>
#include <vector>
#include <map>
#include "ServerConfig.hpp"
#include "Server.hpp"
#include "ClientHandler.hpp"

class Server;

class ServersMonitor
{
public:
    ServersMonitor(const std::vector<ServerConfig> &serverConfigs);
    ~ServersMonitor();

    void run();
	void printFdSet(fd_set set);

private:
    ServersMonitor();
    ServersMonitor(const ServersMonitor &src);
    ServersMonitor &operator=(const ServersMonitor &src);

    bool portInUse(int port);
    void addServer(const ServerConfig &serverConfig);
    Server *getServerFromPort(int port);
    Server *getServerFromSocket(int socket);
    Server *getServerForClient(int clientSocket);
	bool isClientSocket(int fd);
    std::map<int, Server> _servers;
    fd_set readSockets;
	fd_set writeSockets;
    int     _fdMax;
};

#endif
