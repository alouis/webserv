/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClientHandler.hpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 10:11:51 by alouis            #+#    #+#             */
/*   Updated: 2021/12/17 22:15:26 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CLIENT_HPP
#define CLIENT_HPP

#include <iostream>
#include <string>
#include <vector>
#include "Server.hpp"
#include "ServerConfig.hpp"
#include "RequestData.hpp"
#include "Response.hpp"
#include "CGIHandler.hpp"

class ServerConfig;

class ClientHandler
{
public:
	ClientHandler();
	ClientHandler(fd_set *readSocketsTemp, fd_set *readSockets, fd_set *writeSocketsTemp, fd_set *writeSockets);
	ClientHandler(const ClientHandler &src);
	ClientHandler &operator=(const ClientHandler &src);
	~ClientHandler();
	void clean();
	std::vector<unsigned char> &getBuffer();
	unsigned long getClientMaxBodySize() const;
	int getSocket() const;
	bool isBufferEmpty() const;
	bool isResponseChunked() const;
	bool	errorRaised() const;

	void setSocket(int newSocket);
	void setConfig(ServerConfig *newConfig);
	void addToBuffer(std::vector<unsigned char> newBuffer);

	void	checkUri();
	int	checkPermissions();
	int openFile();
	void readFile();
	int chunkResponse();
	int readFileInChunk();
	int loadDataInFile(t_postElement post);
	int errorReadingFile();
	void closeFile();
	void clearRequest();
	void clearResponse();
	void printBuffer() const;
	bool requestIsOver() const;
	void	createExpectedResponse();
	int	checkErrorConfig(int error);

	bool handleCGI();
	bool isCGIRunning();
	// check max body size -> bool clientMaxBodySize() -> compare server config & request body size

	RequestData _req;
	fd_set *_readSocketsTemp;
	fd_set *_readSockets;
	fd_set *_writeSocketsTemp;
	fd_set *_writeSockets;
	std::string	_initialUri;

private:

	CGIHandler _cgiHandler;
	int _socket;
	std::vector<unsigned char> _buffer;
	ServerConfig *_config;
	bool _responseIsChunked;
	std::ifstream _file;
	std::streampos _positionInFile;

	void transferCGIResponseToBuffer();
};

std::ostream &operator<<(std::ostream &os, ClientHandler &rh);

#endif
