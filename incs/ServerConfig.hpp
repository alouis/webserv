/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ServerConfig.hpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/26 11:32:06 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 19:47:59 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVERCONFIG_HPP
#define SERVERCONFIG_HPP
#include "Routes.hpp"
#include "tcpServer.hpp"
#include <vector>
#include <sys/socket.h>
#include <sys/select.h>
#include <arpa/inet.h>
#include "ErrorPage.hpp"

class Routes;

class ServerConfig
{

	class BadHostFormatException : public std::exception
	{
		virtual const char *what() const throw()
		{
			return "Bad Host Format";
		}
	};

public:
	friend class ServerConfigParser;

	ServerConfig(); //For mocked data
	ServerConfig(
		bool def, int port, std::string host, std::vector<std::string> serverNames, std::string root,
		int clientMaxBodySize, errorPage defaultErrorPage, std::vector<Routes> routes);
	ServerConfig(ServerConfig const &);
	ServerConfig &operator=(ServerConfig const &);
	virtual ~ServerConfig();

	int getPort() const;
	in_port_t getPortInNetworkByteOrder() const;
	std::string const &getHost() const;
	in_addr_t getHostInNetworkByteOrder() const throw(BadHostFormatException);
	std::vector<std::string> const &getServerNames() const;
	unsigned long getClientMaxBodySize() const;
	std::string const &getRoot() const;
	bool getDefault() const;
	errorPage const &getDefaultErrorPage() const;
	std::vector<Routes> const &getRoutes() const;

protected:
	bool _default;
	int _port;
	std::string _host;
	std::vector<std::string> _serverNames;
	std::string _root;
	unsigned long _clientMaxBodySize;
	// std::string _defaultErrorPage;
	errorPage _defaultErrorPage;
	std::vector<Routes> _routes;
};

std::ostream &operator<<(std::ostream &stream, ServerConfig const &cl);

typedef std::vector<ServerConfig> ServerConfigs;

#endif
