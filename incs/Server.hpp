/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Server.hpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 10:51:47 by alouis            #+#    #+#             */
/*   Updated: 2021/12/18 10:15:01 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef SERVER_HPP
#define SERVER_HPP
#define MAX_PRINT 10000

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include "ServerConfig.hpp"
#include "ClientHandler.hpp"

class ServerConfig;
class ClientHandler;

class Server
{
    class ServerSetUpException : public std::exception
    {
        virtual const char *what() const throw()
        {
            return "\033[1;33mSetting server up failed\033[0m\n";
        }
    };

    class SocketBindingException : public std::exception
    {
        virtual const char *what() const throw()
        {
            return "\033[1;33mBinding server's socket failed\033[0m\n";
        }
    };

    friend class ServersMonitor;

public:
    Server();
    Server(const ServerConfig &serverConfigs);
    Server(const Server &src);
    ~Server();

    int getServerSocket() const;
    int getPort() const;
    std::vector<ServerConfig> const &getServerConfigs() const;
    int processRequest(int socket);

private:
    Server &operator=(const Server &src);

    ServerConfig *getDefaultConfig();
    void setServerSocket(int socket);
    int establishClientConnection(fd_set *readSocketsTemp, fd_set *readSockets, fd_set *writeSocketsTemp, fd_set *writeSockets); // probably returns int with error code in case connection didnt work
    int establishServerConnection(const ServerConfig &conf);
    int readParseRequest(int socket);
    void checkConfig(int clientSocket, std::string const &host);
    void    buildErrorReponse(int socket);
    int executeAndRespond(int socket);
    void executeRequest(int socket);
    void executeGet(int socket);
    void executePost(int socket);
    void executeDelete(int socket);
    void eraseClient(int client);
    void printClientResponse(int socket);

    int _port;
    int _serverSocket;
    std::vector<ServerConfig> _serverConfigs;
    std::map<int, ClientHandler> _clients;

};

std::ostream &operator<<(std::ostream &os, Server const &rh);

#endif
