/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tcpServer.hpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/25 10:22:14 by alouis            #+#    #+#             */
/*   Updated: 2021/12/17 11:14:50 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef TCPSERVER_HPP
#define TCPSERVER_HPP

// #include <string>
// #include <iostream>
// #include <cstring>
// #include <csignal>
// #include <sys/socket.h>
// #include <sys/select.h>
// #include <arpa/inet.h>
// #include <strings.h>
// #include <vector>
// #include <iterator>
// #include <stdlib.h>
// #include <stdio.h>
// #include <unistd.h>
// #include <fcntl.h>
// #include <fstream>
// #include "Response.hpp"
// #include "RequestData.hpp"
// #include "Server.hpp"
// #include "ServerConfig.hpp"
// #include "ServerConfigParser.hpp"
// #include "ServersMonitor.hpp"
// #include "CGIHandler.hpp"

#define SERVER_PORT 18000
#define MAXLINE 4056

extern bool on;

// void	parseRequest( std::string requestStr, RequestData &requestObj );
bool checkArguments(int argc, char *argv[]);
// int readSocket(int socket);

#endif
