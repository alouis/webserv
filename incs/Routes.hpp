#ifndef ROUTES_HPP
#define ROUTES_HPP
#include <iostream>
#include <string>
#include <vector>
#include "ServerConfigParser.hpp"
#include "ErrorPage.hpp"
#include "RewriteOptions.hpp"

class Routes
{
public:
	Routes();
	~Routes();
	Routes(Routes const &);
	Routes &operator=(Routes const &);

	std::string const &getRoutePath() const;
	std::vector<std::string> const &getHTTPMethodsAccepted() const;
	rewriteOption const &getHTTPRedirection() const;
	std::string const &getRoot() const;
	std::string const &getDefaultFileIfDirectoryRequest() const;
	std::vector<std::string> const &getCGIExtensions() const;
	std::string const &getFileUploadDirectory() const;
	errorPage const &getErrorPage() const;
	bool getAutoIndex() const;

protected:
	friend class ServerConfigParser;
	std::string _routePath;
	std::vector<std::string> _HTTPMethodsAccepted;
	rewriteOption _HTTPRedirection;
	std::string _root;
	std::string _defaultFileIfDirectoryRequest; //NULL if directory listing
	std::vector<std::string> _CGIExtensions;
	std::string _fileUploadDirectory;			//NULL if dont accept files
	errorPage _errorPage;
	bool _autoIndex;
};

std::ostream &operator<<(std::ostream &stream, Routes const &cl);

#endif
