/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CGIHandler.hpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/07 22:33:35 by mroux             #+#    #+#             */
/*   Updated: 2021/12/18 10:47:43 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef CGIHANDLER_HPP
#define CGIHANDLER_HPP
#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unistd.h>
#include <sys/wait.h>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/errno.h>
#include <fcntl.h>
#include "RequestData.hpp"

#define CGI_BUFFER_LENGTH 100

extern char **env;

class CGIHandler
{
public:
	class CGIException : public std::exception
	{
		virtual const char *what() const throw()
		{
			return "CGI Error";
		}
	};

	static bool isCGISupportNeeded(std::string const&uri, std::vector<std::string> const &extensionsAllowed);

	CGIHandler();
	~CGIHandler();
	CGIHandler(CGIHandler const &);
	CGIHandler &operator=(CGIHandler const &);
	void runCGIRequest(RequestData const &req, fd_set *readSockets, fd_set *writeSockets);
	std::vector<unsigned char> const &getCGIResponse();
	std::vector<unsigned char> extractHeader();
	std::vector<unsigned char> extractBody();
	size_t getContentLength();
	void readData(fd_set *readSocketsTemp, fd_set *readSockets);
	void writeData(uchar_vector data, fd_set *writeSocketsTemp);
	bool isRunning() const;
	int getFd() const;
	void closeWritePipe(fd_set *writeSockets);
private:
	enum fileType
	{
		PYTHON = 0,
		PHP,
		NOT_ALLOWED,
	};
	typedef fileType fileType;

	static std::string findFileType(std::string const& uri);
	char **buildArgs();
	void extractGlobalVariables();
	void buildEnv();
	void execCGI();

	std::map<std::string, std::string> _cgiPaths;
	std::map<std::string, std::string> _globalVariables;
	std::vector<std::string> _extensionMapping;
	char ** _env;
	std::vector<unsigned char> _cgiResponse;
	int _fdsOut[2];
	int _fdsIn[2];
	bool _isCGIRunning;
	std::string _uri;
	RequestData _req;
	bool _isChunked;
};

std::ostream &operator<<(std::ostream &stream, CGIHandler const &cl);

#endif
