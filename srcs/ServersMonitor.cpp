/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ServersMonitor.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 16:53:16 by alouis            #+#    #+#             */
/*   Updated: 2021/12/16 21:14:39 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ServersMonitor.hpp"

ServersMonitor::ServersMonitor() {}

ServersMonitor::ServersMonitor(const std::vector<ServerConfig> &serverConfigs)
{
	_fdMax = 2;
	// create each server based on infos from config file (or add config to object server if port is already in use) and stock them with id = socket's number
	for (ServerConfigs::const_iterator newConfig = serverConfigs.begin(); newConfig != serverConfigs.end(); newConfig++)
	{
		if (!portInUse(newConfig->getPort()))
		{
			try
			{
				addServer(*newConfig);
			}
			catch (const std::exception &e)
			{
				std::cerr << e.what() << '\n';
				on = false;
			}
		}
		else
			getServerFromPort(newConfig->getPort())->_serverConfigs.push_back(*newConfig);
	}
	// put servers' socket in fd_set being watched by select()
	FD_ZERO(&readSockets);
	FD_ZERO(&writeSockets);
	for (std::map<int, Server>::iterator serverSocket = _servers.begin(); serverSocket != _servers.end(); serverSocket++)
	{
		FD_SET(serverSocket->first, &readSockets);
		if (_fdMax < serverSocket->first)
			_fdMax = serverSocket->first;
	}
	// print each server's infos
	for (std::map<int, Server>::iterator it = _servers.begin(); it != _servers.end(); it++)
		std::cout << it->second << std::endl
				  << std::endl;
}

ServersMonitor::ServersMonitor(const ServersMonitor &src)
{
	*this = src;
}

ServersMonitor &ServersMonitor::operator=(const ServersMonitor &src)
{
	(void)src;
	return *this;
}

ServersMonitor::~ServersMonitor()
{
	for (std::map<int, Server>::iterator sv = _servers.begin(); sv != _servers.end(); sv++)
	{
		std::cout << "\033[1;33mClosing server's socket " << sv->second.getServerSocket() << "\033[0m\n";
		close(sv->second.getServerSocket());
		for (std::map<int, ClientHandler>::iterator cl = sv->second._clients.begin(); cl != sv->second._clients.end(); cl++)
		{
			std::cout << "and closing client's socket " << cl->second.getSocket() << std::endl;
			close(cl->first);
			cl->second.closeFile();	// in case chunked or cropped response
		}
	}
}

void ServersMonitor::printFdSet(fd_set set)
{
	int fd = 0;
	std::cout << "Print set:" << std::endl;
	while (fd < FD_SETSIZE)
	{
		if (FD_ISSET(fd, &set))
			std::cout << fd << " ; ";
		fd++;
	}
	std::cout << std::endl;
}

bool ServersMonitor::portInUse(int port)
{
	for (std::map<int, Server>::iterator existingServer = _servers.begin(); existingServer != _servers.end(); existingServer++)
	{
		if (existingServer->second.getPort() == port)
			return (true);
	}
	return (false);
}
void ServersMonitor::addServer(const ServerConfig &serverConfig)
{
	Server newServer;

	newServer._serverConfigs.push_back(serverConfig);
	newServer._serverSocket = newServer.establishServerConnection(serverConfig);
	_servers.insert(std::make_pair(newServer.getServerSocket(), newServer));
}

Server *ServersMonitor::getServerFromPort(int port)
{
	for (std::map<int, Server>::iterator server = _servers.begin(); server != _servers.end(); server++)
	{
		if (server->second.getPort() == port)
			return (&server->second);
	}
	return NULL;
}

Server *ServersMonitor::getServerFromSocket(int socket)
{
	for (std::map<int, Server>::iterator server = _servers.begin(); server != _servers.end(); server++)
	{
		if (server->first == socket)
			return (&server->second);
	}
	return NULL;
}

Server *ServersMonitor::getServerForClient(int clientSocket)
{
	for (std::map<int, Server>::iterator server = _servers.begin(); server != _servers.end(); server++)
	{
		if (server->second._clients.find(clientSocket) != server->second._clients.end())
			return (&server->second);
	}
	return NULL;
}

bool ServersMonitor::isClientSocket(int i)
{
	return (getServerFromSocket(i) != NULL || getServerForClient(i) != NULL);
}

void ServersMonitor::run()
{
	fd_set readSocketsTemp;
	fd_set writeSocketsTemp;

	struct timeval timeout;
	Server *server;
	int clientSocket;

	timeout.tv_sec = 4;
	timeout.tv_usec = 0;
	while (1)
	{
		readSocketsTemp = readSockets;
		writeSocketsTemp = writeSockets;

		// std::cout << "===" << std::endl;
		// std::cout << "reading:\n";
		// printFdSet(readSocketsTemp);
		// std::cout << "writing:\n";
		// printFdSet(writeSocketsTemp);
		std::cout << "===" << std::endl;
		if (select(FD_SETSIZE, &readSocketsTemp, &writeSocketsTemp, NULL, &timeout) < 0)
		{
			if (on == true)
				std::cout << "select() failed, closing servers" << std::endl;
			std::cout << strerror(errno) << std::endl;
			on = false;
			break;
		}
		for (int i = 3; i <= FD_SETSIZE; i++)
		{
			clientSocket = 0;
			if (isClientSocket(i) && FD_ISSET(i, &readSocketsTemp))
			{
				if ((server = getServerFromSocket(i)) && (clientSocket = server->establishClientConnection(&readSocketsTemp, &readSockets, &writeSocketsTemp, &writeSockets)))
				{
					#ifdef PRINT
					std::cout << "Client connection established on socket: " << clientSocket << "\n";
					#endif
					FD_SET(clientSocket, &readSockets);
					if (_fdMax < clientSocket)
						_fdMax = clientSocket;
				}
				else
				{
					#ifdef PRINT
					std::cout << "\nSocket n." << i << " ready for reading\n";
					#endif
					if (getServerForClient(i)->processRequest(i))
						FD_SET(i, &writeSockets);
					else
					{
						#ifdef PRINT
						std::cout << "Clearing socket n." << i << " from reading\n";
						#endif
						// sockets cleared in clientHandler
						if (i == _fdMax)
							_fdMax--;
					}
				}
			}
			if (isClientSocket(i) && FD_ISSET(i, &writeSocketsTemp))
			{
				#ifdef PRINT
				std::cout << "\nSocket n." << i << " ready for writing\n";
				#endif
				if (!getServerForClient(i)->executeAndRespond(i))
					FD_CLR(i, &writeSockets);
			}
		}
	}
}
