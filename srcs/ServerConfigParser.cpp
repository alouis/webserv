/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ServerConfigParser.cpp                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/26 11:56:30 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 20:47:48 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ServerConfigParser.hpp"

ServerConfigParser::ServerConfigParser() : _configPath()
{
}

ServerConfigParser::ServerConfigParser(std::string configPath) : _configPath(configPath)
{
}

ServerConfigParser::~ServerConfigParser()
{
	_src.close();
}

ServerConfigParser::ServerConfigParser(ServerConfigParser const &cl)
{
	operator=(cl);
}

ServerConfigParser &ServerConfigParser::operator=(ServerConfigParser const &cl)
{
	_configPath = cl._configPath;
	return (*this);
}

std::vector<std::string> ServerConfigParser::extractAttribute(std::string const &content, std::string const &attributeName) const
{
	std::istringstream strm(content);
	std::string line;
	std::string::iterator it;
	std::string attr, sub;
	std::vector<std::string> ret;

	while (getline(strm, line))
	{
		size_t pos;
		if ((pos = line.find(attributeName)) != std::string::npos)
		{
			sub = line.substr(pos);
			it = sub.begin();
			for (; !isspace(*it) && it < sub.end(); it++)
				;
			while (*it != ';' && it != sub.end())
			{
				attr = "";
				while (isspace(*it) && it < sub.end())
					it++;
				while (*it != ';' && !isspace(*it) && it < sub.end())
					attr.push_back(*it++);
				ret.push_back(attr);
			}
			return ret;
		}
	}
	return std::vector<std::string>();
}

std::vector<std::string> ServerConfigParser::extractRoutesContent(std::string const &content) const
{
	std::istringstream strm(content);
	std::string line;
	std::string routeContent;
	std::vector<std::string> routeContents;

	while (getline(strm, line))
	{
		if (line.find("location") != std::string::npos)
		{
			routeContent.clear();
			routeContent += (line + "\n");
			while (std::getline(strm, line))
			{
				routeContent += (line + "\n");
				if (line.find("}") != std::string::npos)
					break;
			}
			routeContents.push_back(routeContent);
		}
	}
	return routeContents;
}

std::vector<std::string> ServerConfigParser::extractContents()
{
	std::string newLine;
	std::vector<std::string> contents;
	std::string content;
	bool extracting = false;
	int numberOfAccolade = 0;

	_src.open(_configPath.c_str(), std::ios::in);
	if (!_src.is_open())
		throw InvalidConfigPathException();

	while (!_src.eof())
	{
		std::getline(_src, newLine);
		if (extracting)
		{
			content += ("\n" + newLine);
			if (newLine.find("{") != std::string::npos)
				numberOfAccolade++;
			else if (newLine.find("}") != std::string::npos)
				numberOfAccolade--;
			if (numberOfAccolade == 0)
			{
				contents.push_back(content);
				extracting = false;
			}
		}
		else if (newLine == "server {")
		{
			content = newLine;
			numberOfAccolade++;
			extracting = true;
		}
	}
	_src.close();
	if (extracting || contents.size() == 0)
		throw InvalidConfigFileException();
	return contents;
}

void ServerConfigParser::extractListen(std::string const &content, ServerConfig &conf) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "listen");
	if (attr.size() >= 1)
		conf._port = atoi(attr[0].c_str());
	if (attr.size() >= 2)
	{
		if (attr[1] == "default_server")
			conf._default = true;
		else
		{
			conf._host = attr[1];
			if (attr.size() >= 3 && attr[2] == "default_server")
				conf._default = true;
		}
	}
}

void ServerConfigParser::extractServerNames(std::string const &content, ServerConfig &conf) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "server_name");
	if (attr.size() >= 1)
		conf._serverNames = attr;
}

void ServerConfigParser::extractRoot(std::string const &content, ServerConfig &conf) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "root");
	if (attr.size() >= 1)
		conf._root = attr[0];
}

void ServerConfigParser::extractClientMaxBodySize(std::string const &content, ServerConfig &conf) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "client_max_body_size");
	if (attr.size() >= 1)
		conf._clientMaxBodySize = atoi(attr[0].c_str());
}

void ServerConfigParser::extractDefaultErrorPage(std::string const &content, ServerConfig &conf) const
{
	std::vector<std::string> attr;
	size_t i;
	errorPage errPage;

	attr = extractAttribute(content, "error_page");
	if (attr.size() < 2)
		throw InvalidConfigFileException();
	for (i = 0; i < attr.size() - 1; i++)
	{
		//not a number
		if (attr[i].find_first_not_of("0123456789") != std::string::npos)
			throw InvalidConfigFileException();
		errPage.codes.push_back(atoi(attr[i].c_str()));
	}
	errPage.path = attr[i];
	conf._defaultErrorPage = errPage;
}

void ServerConfigParser::extractRoutePath(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "location");
	if (attr.size() < 2 || attr[1] != "{")
		throw InvalidConfigFileException();
	route._routePath = attr[0];
}

void ServerConfigParser::extractRouteHTTPMethod(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	//TODO: use the same name than nginx
	attr = extractAttribute(content, "HTTP_methods");
	if (attr.size() >= 1)
		route._HTTPMethodsAccepted = attr;
}

void ServerConfigParser::extractRouteHTTPRedirection(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;
	attr = extractAttribute(content, "rewrite");
	if (attr.size() >= 2)
	{
		route._HTTPRedirection.from = attr[0];
		route._HTTPRedirection.to = attr[1];
	}
}

void ServerConfigParser::extractRouteRoot(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "root");
	if (attr.size() >= 1)
		route._root = attr[0];
}

void ServerConfigParser::extractRouteDefaultFileIfDirectoryRequest(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "index");
	if (attr.size() >= 1)
		route._defaultFileIfDirectoryRequest = attr[0];
}

void ServerConfigParser::extractRouteCGIExtension(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "cgi_extensions");
	if (attr.size() >= 1)
		route._CGIExtensions = attr;
}
void ServerConfigParser::extractRouteFileUploadDirectory(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "upload_store");
	if (attr.size() >= 1)
		route._fileUploadDirectory = attr[0];
}
void ServerConfigParser::extractRouteErrorPage(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;
	size_t i;
	errorPage errPage;

	attr = extractAttribute(content, "error_page");
	if (attr.size() == 0)
		return;
	if (attr.size() < 2)
		throw InvalidConfigFileException();
	for (i = 0; i < attr.size() - 1; i++)
	{
		//not a number
		if (attr[i].find_first_not_of("0123456789") != std::string::npos)
			throw InvalidConfigFileException();
		errPage.codes.push_back(atoi(attr[i].c_str()));
	}
	errPage.path = attr[i];
	route._errorPage = errPage;
}

void ServerConfigParser::extractAutoindex(std::string const &content, Routes &route) const
{
	std::vector<std::string> attr;

	attr = extractAttribute(content, "autoindex");
	if (attr.size() >= 1 && attr[0].compare("true") == 0)
		route._autoIndex = true;
	else
		route._autoIndex = false;
}

void ServerConfigParser::extractRoutes(std::string const &content, ServerConfig &conf) const
{
	Routes route;
	std::vector<std::string> routeContents;

	routeContents = extractRoutesContent(content);
	for (std::vector<std::string>::iterator it = routeContents.begin(); it != routeContents.end(); it++)
	{
		extractRoutePath(*it, route);
		extractRouteRoot(*it, route);
		extractRouteErrorPage(*it, route);
		extractRouteDefaultFileIfDirectoryRequest(*it, route);
		extractRouteFileUploadDirectory(*it, route);
		extractRouteHTTPMethod(*it, route);
		extractRouteHTTPRedirection(*it, route);
		extractRouteCGIExtension(*it, route);
		extractAutoindex(*it, route);
		conf._routes.push_back(route);
	}
}

ServerConfig ServerConfigParser::extractConfig(std::string const &content) const
{
	ServerConfig conf;

	extractListen(content, conf);
	extractServerNames(content, conf);
	extractRoot(content, conf);
	extractClientMaxBodySize(content, conf);
	extractDefaultErrorPage(content, conf);
	extractRoutes(content, conf);
	return conf;
}

void ServerConfigParser::checkConfigs(ServerConfigs &configs)
{
	// Set only one default
	int defId = -1;
	int i = 0;
	for (ServerConfigs::iterator it = configs.begin(); it < configs.end(); it++, i++)
	{
		if (it->_default)
		{
			if (defId != -1)
				it->_default = false;
			else
				defId = i;
		}
	}
	if (defId == -1)
		configs[0]._default = true;
}

ServerConfigs ServerConfigParser::buildServerConfigs()
{
	ServerConfigs configs;
	std::vector<std::string> contents;

	if (_configPath.empty())
		return buildMockData();
	contents = extractContents();
	for (std::vector<std::string>::iterator it = contents.begin(); it < contents.end(); it++)
	{
		ServerConfig conf = extractConfig(*it);
		configs.push_back(conf);
	}
	checkConfigs(configs);

	std::cout << "Extracted configs : " << std::endl;
	for (ServerConfigs::iterator it = configs.begin(); it != configs.end(); it++)
		std::cout << *it << std::endl;
	return configs;
}

ServerConfigs ServerConfigParser::buildMockData() const
{
	ServerConfigs lst;
	lst.push_back(ServerConfig(true, SERVER_PORT, "127.0.0.1", std::vector<std::string>(), "/", 100000, errorPage(), std::vector<Routes>()));
	return lst;
}
