/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ErrorPage.cpp                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/26 21:55:58 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 20:48:51 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ErrorPage.hpp"

std::ostream &operator<<(std::ostream &stream, errorPage const &cl)
{
	for (size_t i = 0; i < cl.codes.size(); i++)
		stream << " " << cl.codes[i];
	stream << " - " << cl.path;

	return stream;
}
