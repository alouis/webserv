/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   CGIHandler.cpp                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/07 22:34:01 by mroux             #+#    #+#             */
/*   Updated: 2021/12/18 11:07:16 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "CGIHandler.hpp"

CGIHandler::CGIHandler() : _isCGIRunning(false), _isChunked(false)
{
	_cgiPaths.insert(std::pair<std::string, std::string>("py", "/usr/local/bin/python3"));
	_cgiPaths.insert(std::pair<std::string, std::string>("php", "/usr/local/bin/php-cgi"));
	_extensionMapping.push_back("py");
	_extensionMapping.push_back("php");
}

CGIHandler::~CGIHandler()
{
}

CGIHandler::CGIHandler(CGIHandler const &cl)
{
	operator=(cl);
}

CGIHandler &CGIHandler::operator=(CGIHandler const &cl)
{
	_cgiPaths = cl._cgiPaths;
	return (*this);
}

std::string CGIHandler::findFileType(std::string const &uri)
{
	std::vector<std::string> extensionMapping;
	extensionMapping.push_back("py");
	extensionMapping.push_back("php");

	for (std::vector<std::string>::iterator it = extensionMapping.begin(); it != extensionMapping.end(); it++)
	{
		if (uri.find("." + *it) != std::string::npos)
			return *it;
	}
	return "";
}

// TODO: does it depend on the folder too ??
bool CGIHandler::isCGISupportNeeded(std::string const &uri, std::vector<std::string> const &extensionsAllowed)
{
	std::string ext = findFileType(uri);
	for (std::vector<std::string>::const_iterator it = extensionsAllowed.begin(); it != extensionsAllowed.end(); it++)
	{
		if (ext.compare(*it) == 0)
			return true;
	}
	return false;
}

int CGIHandler::getFd() const
{
	return _fdsOut[0];
}

char **CGIHandler::buildArgs()
{
	char **argv;
	std::string ext = findFileType(_uri);

	argv = (char **)malloc(sizeof(char *) * 3);
	argv[0] = strdup(_cgiPaths[ext].c_str());
	argv[1] = strdup((_globalVariables["SCRIPT_FILENAME"]).c_str());
	argv[2] = NULL;
	return argv;
}

void CGIHandler::extractGlobalVariables()
{
	std::string ext = findFileType(_uri);
	_globalVariables["REQUEST_URI"] = _uri;
	_globalVariables["SCRIPT_NAME"] = _uri.substr(0, _uri.find("." + ext) + ext.size() + 1);
	_globalVariables["PATH_INFO"] = _uri.substr(_uri.find("." + ext) + ext.size() + 1);
	_globalVariables["QUERY_STRING"] = _uri.find("?") != std::string::npos ? _uri.substr(_uri.find("?") + 1) : "";
	_globalVariables["SCRIPT_FILENAME"] = "./" + _uri.substr(0, _uri.find("." + ext) + ext.size() + 1);
	_globalVariables["REQUEST_METHOD"] = _req.getMethod().c_str();
	_globalVariables["CONTENT_TYPE"] = _req.getContentType().c_str();
	_globalVariables["CONTENT_LENGTH"] = std::to_string(_req.getMessageBody().size());
}

void CGIHandler::buildEnv()
{
	size_t len = 0;
	while (env[len] != NULL)
		len++;
	_env = (char **)malloc(sizeof(char *) * (len + 9));
	len = 0;
	while (env[len] != NULL)
	{
		_env[len] = strdup(env[len]);
		len++;
	}

	_env[len++] = strdup(("REDIRECT_STATUS=1")); // security reasons
	_env[len++] = strdup(("REQUEST_URI=" + _globalVariables["REQUEST_URI"]).c_str());
	_env[len++] = strdup(("SCRIPT_NAME=" + _globalVariables["SCRIPT_NAME"]).c_str());
	_env[len++] = strdup(("PATH_INFO=" + _globalVariables["PATH_INFO"]).c_str());
	_env[len++] = strdup(("QUERY_STRING=" + _globalVariables["QUERY_STRING"]).c_str());
	_env[len++] = strdup(("SCRIPT_FILENAME=" + _globalVariables["SCRIPT_FILENAME"]).c_str());
	_env[len++] = strdup(("REQUEST_METHOD=" + _globalVariables["REQUEST_METHOD"]).c_str());
	_env[len++] = strdup(("CONTENT_TYPE=" + _globalVariables["CONTENT_TYPE"]).c_str());
	_env[len++] = strdup(("CONTENT_LENGTH=" + _globalVariables["CONTENT_LENGTH"]).c_str());
	_env[len] = NULL;
}

void CGIHandler::execCGI()
{
	extractGlobalVariables();
	buildEnv();
	char **argv = buildArgs();

	// size_t len = 0;
	// while (_env[len] != NULL)
	// 	std::cout << _env[len++] << std::endl;
	close(_fdsIn[1]);
	dup2(_fdsIn[0], STDIN_FILENO);
	dup2(_fdsOut[1], STDOUT_FILENO);
	if (execve(argv[0], argv, _env) == -1)
		throw CGIException();
}

void CGIHandler::writeData(uchar_vector data, fd_set *writeSocketsTemp)
{
	int fd = _fdsIn[1];
	if (!FD_ISSET(fd, writeSocketsTemp))
		return;
	for (uchar_vector::iterator it = data.begin(); it != data.end(); it++)
		write(fd, &(*it), 1);
}

void CGIHandler::closeWritePipe(fd_set *writeSockets)
{
	close(_fdsIn[1]);
	FD_CLR(_fdsIn[1], writeSockets);
}
void CGIHandler::readData(fd_set *readSocketsTemp, fd_set *readSockets)
{
	size_t bytesRead = 0;
	struct timeval timeout;
	timeout.tv_sec = 0;
	timeout.tv_usec = 1000;
	char *buffer = (char *)calloc(CGI_BUFFER_LENGTH, 1);
	int fd = _fdsOut[0];

	if (!FD_ISSET(fd, readSocketsTemp))
		return;
	bytesRead = read(fd, buffer, CGI_BUFFER_LENGTH);
	if (bytesRead == 0)
	{
		_isCGIRunning = false;
		close(fd);
		FD_CLR(fd, readSockets);
		return;
	}
	for (size_t i = 0; i < bytesRead; i++)
		_cgiResponse.push_back(buffer[i]);
}

void CGIHandler::runCGIRequest(RequestData const &req, fd_set *readSockets, fd_set *writeSockets)
{
	pid_t pid;
	_req = req;
	_uri = req.getRequestUri();
	_cgiResponse.clear();

	if (_req.getIsChunked())
	{
		_isChunked = true;
		return;
	}

	pipe(_fdsOut);
	pipe(_fdsIn);

	if ((pid = fork()) < 0)
		throw CGIException();
	else if (pid == 0)
		execCGI();
	close(_fdsIn[0]);
	close(_fdsOut[1]);
	FD_SET(_fdsOut[0], readSockets);
	if (req.getMethod() == "POST")
		FD_SET(_fdsIn[1], writeSockets);
	else
		close(_fdsIn[1]);
	_isCGIRunning = true;
}

bool CGIHandler::isRunning() const
{
	return _isCGIRunning;
}

std::vector<unsigned char> const &CGIHandler::getCGIResponse()
{
	return _cgiResponse;
}

std::vector<unsigned char> CGIHandler::extractHeader()
{
	std::vector<unsigned char> header;
	if (_cgiResponse.size() == 0)
		return header;
	std::string responseAsString = (char *)&_cgiResponse[0];
	size_t endOfHeaderPos = responseAsString.find("\r\n\r\n");
	if (endOfHeaderPos != std::string::npos)
		header.insert(header.begin(), _cgiResponse.begin(), _cgiResponse.begin() + endOfHeaderPos + 2);
	return header;
}

std::vector<unsigned char> CGIHandler::extractBody()
{
	std::vector<unsigned char> body;
	if (_cgiResponse.size() == 0)
		return body;
	std::string responseAsString = (char *)&_cgiResponse[0];
	size_t endOfHeaderPos = responseAsString.find("\r\n\r\n");
	if (endOfHeaderPos != std::string::npos)
		body.insert(body.begin(), _cgiResponse.begin() + endOfHeaderPos + 4, _cgiResponse.end());
	return body;
}

size_t CGIHandler::getContentLength()
{
	return extractBody().size();
}

std::ostream &operator<<(std::ostream &stream, CGIHandler const &cl)
{
	(void)cl;
	stream << "Hello" << std::endl;
	return (stream);
}
