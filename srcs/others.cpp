/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   others.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/23 12:40:47 by alouis            #+#    #+#             */
/*   Updated: 2021/10/19 17:12:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ServerConfig.hpp"
#include "ServerConfigParser.hpp"
#include "tcpServer.hpp"
#include "Response.hpp"

bool checkArguments(int argc, char *argv[])
{
	(void)argv;
	return (argc == 2);
}

