/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Routes.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/26 13:23:00 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 20:47:04 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Routes.hpp"

Routes::Routes()
{
}

Routes::~Routes()
{
}

Routes::Routes(Routes const &cl)
{
	operator=(cl);
}

Routes &Routes::operator=(Routes const &cl)
{
	_routePath = cl._routePath;
	_HTTPMethodsAccepted = cl._HTTPMethodsAccepted;;
	_HTTPRedirection = cl._HTTPRedirection;
	_root = cl._root;
	_defaultFileIfDirectoryRequest = cl._defaultFileIfDirectoryRequest;
	_CGIExtensions = cl._CGIExtensions;
	_fileUploadDirectory = cl._fileUploadDirectory;
	_errorPage = cl._errorPage;
	_autoIndex = cl._autoIndex;
	return (*this);
}

std::string const &Routes::getRoutePath() const { return _routePath; }
std::vector<std::string> const &Routes::getHTTPMethodsAccepted() const { return _HTTPMethodsAccepted; }
rewriteOption const &Routes::getHTTPRedirection() const { return _HTTPRedirection; }
std::string const &Routes::getRoot() const {return _root; }
std::string const &Routes::getDefaultFileIfDirectoryRequest() const { return _defaultFileIfDirectoryRequest; }
std::vector<std::string> const &Routes::getCGIExtensions() const { return _CGIExtensions; }
std::string const &Routes::getFileUploadDirectory() const { return _fileUploadDirectory; }
errorPage const &Routes::getErrorPage() const {return _errorPage; }
bool Routes::getAutoIndex() const { return _autoIndex; };

std::ostream &operator<<(std::ostream &stream, Routes const &cl)
{
	stream << "---------- Route -------------" << std::endl;
	stream << "Path: " << cl.getRoutePath() << std::endl;
	std::cout << "HTTP methods accepted: ";
	for (std::vector<std::string>::const_iterator method = cl.getHTTPMethodsAccepted().begin(); method != cl.getHTTPMethodsAccepted().end(); method++)
		std::cout << *method << " - ";
	std::cout << std::endl;
	std::cout << "HTTP Redirection: " << cl.getHTTPRedirection().from << " -> " << cl.getHTTPRedirection().to << std::endl;
	std::cout << "Root: " << cl.getRoot() << std::endl;
	std::cout << "Directory request: " << cl.getDefaultFileIfDirectoryRequest() << std::endl;
	std::cout << "CGI extensions: ";
	for (std::vector<std::string>::const_iterator ext = cl.getCGIExtensions().begin(); ext != cl.getCGIExtensions().end(); ext++)
		std::cout << *ext << " - ";
	std::cout << std::endl;
	std::cout << "File upload dir: " << cl.getFileUploadDirectory() << std::endl;
	std::cout << "Error page:" << cl.getErrorPage() << std::endl;
	std::cout << "Autoindex: " << cl.getAutoIndex() << std::endl;

	return (stream);
}
