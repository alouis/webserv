/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Server.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/10/18 11:04:33 by alouis            #+#    #+#             */
/*   Updated: 2021/12/17 16:14:01 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Server.hpp"

Server::Server() : _serverSocket(0) {}

Server::Server(const Server &src)
{
	*this = src;
}

Server &Server::operator=(const Server &src)
{
	if (this != &src)
	{
		this->_port = src._port;
		this->_serverSocket = src._serverSocket;
		this->_serverConfigs = src._serverConfigs;
		this->_clients = src._clients;
	}
	return (*this);
}

Server::~Server() {}

void Server::setServerSocket(int socket)
{
	_serverSocket = socket;
}

int Server::getServerSocket() const { return _serverSocket; }
int Server::getPort() const { return _port; }
std::vector<ServerConfig> const &Server::getServerConfigs() const { return (_serverConfigs); }

ServerConfig *Server::getDefaultConfig()
{
	for (std::vector<ServerConfig>::iterator config = _serverConfigs.begin(); config != _serverConfigs.end(); config++)
	{
		if (config->getDefault() == true)
			return (&(*config));
	}
	return (&(*_serverConfigs.begin()));
}

int Server::establishServerConnection(const ServerConfig &conf)
{
	int ret;
	int listeningSocket;
	struct sockaddr_in servAddr;
	int enable = 1;

	if ((listeningSocket = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		throw ServerSetUpException();
	// Enable reusing socket when server just restarted
	if (setsockopt(listeningSocket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
		throw ServerSetUpException();
	if ((ret = fcntl(listeningSocket, F_SETFL, O_NONBLOCK)) < 0)
		throw ServerSetUpException();
	bzero(&servAddr, sizeof(servAddr));
	servAddr.sin_family = AF_INET;
	servAddr.sin_port = conf.getPortInNetworkByteOrder();
	servAddr.sin_addr.s_addr = conf.getHostInNetworkByteOrder();
	if (bind(listeningSocket, (struct sockaddr *)&servAddr, sizeof(servAddr)) < 0)
		throw SocketBindingException();
	if (listen(listeningSocket, 5) < 0)
		throw ServerSetUpException();
	_port = conf.getPort();
	return (listeningSocket);
}

int Server::establishClientConnection(fd_set *readSocketsTemp, fd_set *readSockets, fd_set *writeSocketsTemp, fd_set *writeSockets)
{
	int newSocket;
	unsigned int cliLen;
	struct sockaddr_in cliAddr;
	ClientHandler newClient(readSocketsTemp, readSockets, writeSocketsTemp, writeSockets);
	int on;

	on = 1;
	cliLen = sizeof(cliAddr);
	newSocket = accept(_serverSocket, (struct sockaddr *)&cliAddr, &cliLen);
	if (newSocket < 0 || (setsockopt(newSocket, SOL_SOCKET, SO_KEEPALIVE, (char *)&on, sizeof(on))) < 0)
	{
		std::cout << "\033[1;33mClient connection failed\033[0m\n";
		return (0);
	}
	newClient.setSocket(newSocket);
	newClient.setConfig(getDefaultConfig());
	_clients[newSocket] = newClient;
	return (newSocket);
}

int Server::processRequest(int socket)
{
	if (!readParseRequest(socket))
		return (0);
	if (_clients[socket].errorRaised() == true)
	{
		buildErrorReponse(socket);
		return (1);
	}
#ifdef PRINT
	std::cout << "==============" << _clients[socket]._req.getBodyLength() << std::endl;
	std::cout << "chunk: " << _clients[socket]._req.getIsChunked() << std::endl;
#endif
	if (!_clients[socket]._req.getIsChunked() && _clients[socket]._req.getBodyLength() > _clients[socket].getClientMaxBodySize())
	{
		_clients[socket]._req.setStatusCode(_clients[socket].checkErrorConfig(413));
		buildErrorReponse(socket);
		return (1);
	}
	if (_clients[socket].isResponseChunked() == false && _clients[socket]._req.getHost().size())
		checkConfig(socket, _clients[socket]._req.getHost());

	// std::cout << "\n\nRequest (parsed):\033[1;35m\n";
	// _clients[socket]._req.printRequest();
	// std::cout << "\033[0m------------\n\n";
	return (1);
}

int Server::readParseRequest(int socket)
{
	unsigned char c_buffer[MAXLINE];
	uchar_vector buffer;
	int n;

	bzero(c_buffer, MAXLINE);
	while ((n = recv(socket, c_buffer, MAXLINE - 1, 0)) > 0)
	{
		if (n)
		{
			buffer.insert(buffer.end(), c_buffer, c_buffer + n); // avoid the check of TotalBytesRead()
			bzero(c_buffer, MAXLINE);
		}
	}
	if (!n)
	{
#ifdef PRINT
		std::cout << "Client n." << socket << " closed connection\n";
#endif
		if (_clients[socket].isResponseChunked() == true)
			_clients[socket].closeFile();
		eraseClient(socket); //closing socket there
		return (0);
	}
#ifdef PRINT
	std::cout << "\n\nRequest (RAW):\033[1;33m\n";
	std::vector<unsigned char>::const_iterator it;
	for (it = buffer.begin(); it != buffer.end() && (it - buffer.begin()) < MAX_PRINT; it++)
		std::cout << *it;
	if (buffer.end() != it)
		std::cout << "...\n\n[Output truncated]";
	std::cout << std::endl;
	std::cout << "\033[0m------------\n\n";
	std::cout << "Status code BEFORE parsing request: " << _clients[socket]._req.getStatusCode() << std::endl;
#endif
	if (buffer.size())
		_clients[socket]._req.parseRequest(buffer);
	else
		_clients[socket]._req.setStatusCode(_clients[socket].checkErrorConfig(400));
#ifdef PRINT
	std::cout << "Status code AFTER parsing request: " << _clients[socket]._req.getStatusCode() << std::endl;
#endif
	return (1);
}

void Server::checkConfig(int socket, std::string const &src)
{
	std::string host(src);
	std::string newHost;
	std::size_t	pos;

	if ((pos = host.find_first_of(":")) != std::string::npos)
		newHost = host.substr(0, pos);
	if (newHost.size())
		host.assign(newHost);
	for (std::vector<ServerConfig>::iterator config = _serverConfigs.begin(); config != _serverConfigs.end(); config++)
	{
		for (std::vector<std::string>::const_iterator serverName = config->getServerNames().begin(); serverName != config->getServerNames().end(); serverName++)
		{
			if (host == *serverName)
			{
				_clients[socket].setConfig(&(*config));
				std::cout << "client " << socket << " changed config for " << *config << std::endl;
			}
		}
	}
}

void Server::buildErrorReponse(int socket)
{
	Response res;

	if (_clients[socket].isBufferEmpty() == true)
	{
		res.setStatusCode(_clients[socket].checkErrorConfig(_clients[socket]._req.getStatusCode()));
		_clients[socket].addToBuffer(res.createResponse());
	}
}

int Server::executeAndRespond(int socket)
{
	size_t writtenByte;

	writtenByte = 0;

	if (_clients[socket].errorRaised() == false)
		executeRequest(socket);
	if (_clients[socket].requestIsOver() == false && !_clients[socket]._req.getExpect())
		return (0);
	if (!_clients[socket].errorRaised() && _clients[socket]._req.getExpect())
	{
		std::cout << "COMING HERE\n";
		_clients[socket].createExpectedResponse();
		_clients[socket].printBuffer();
	}
	if (_clients[socket].getBuffer().size() == 0) // this condition if for CGI
		return (1);
	if ((writtenByte = send(socket, &(_clients[socket].getBuffer())[0], _clients[socket].getBuffer().size(), 0)) < 0)
	{
		std::cout << "\033[1;33mError when writing in new socket\033[0m\n";
		return (0);
	}
#ifdef PRINT
	printClientResponse(socket);
#endif
	// std::cout << "sizes sent: " << writtenByte << "/" << _clients[socket].getBuffer().size() << std::endl;
	// std::cout << "------------\n\n";
	if (writtenByte < _clients[socket].getBuffer().size())
	{
		std::vector<unsigned char>::iterator newBegin(_clients[socket].getBuffer().begin());

		while (writtenByte)
		{
			newBegin++;
			writtenByte--;
		}
		_clients[socket].getBuffer().erase(_clients[socket].getBuffer().begin(), newBegin);
		return (1);
	}
	_clients[socket].clearResponse();
	if (!_clients[socket].isResponseChunked() && _clients[socket].requestIsOver() && !_clients[socket].isCGIRunning())
	{
		_clients[socket].clearRequest();
		return (0);
	}
	return (1);
}

void Server::executeRequest(int socket)
{
	_clients[socket].checkUri();
	if (!_clients[socket]._req.arePermissionsChecked() && !_clients[socket].checkPermissions())
		return;
	if (_clients[socket].handleCGI())
		return;
	if (_clients[socket]._req.getMethod() == "GET")
		executeGet(socket);
	else if (_clients[socket]._req.getMethod() == "POST")
		executePost(socket);
	else if (_clients[socket]._req.getMethod() == "DELETE")
		executeDelete(socket);
}

void Server::executeGet(int socket)
{
	if (_clients[socket].isResponseChunked() == true)
	{
		_clients[socket].readFileInChunk();
		return;
	}
	else
	{
		if (!_clients[socket].openFile())
			return;
		else
			_clients[socket].readFile();
	}
}

void Server::executePost(int socket)
{
	Response res;

	if (_clients[socket]._req.getPostElements().begin() != _clients[socket]._req.getPostElements().end())
	{
		for (std::vector<t_postElement>::iterator it = _clients[socket]._req.getPostElements().begin(); it != _clients[socket]._req.getPostElements().end(); it++)
		{
			if (!_clients[socket].loadDataInFile(*it))
			{
				_clients[socket].clearResponse();
				res.setStatusCode(_clients[socket].checkErrorConfig(415));
				_clients[socket].addToBuffer(res.createResponse());
			}
		}
		if (_clients[socket]._req.getIsCropped() == true)
			_clients[socket]._req.clearLastDataPostElem();
		if (_clients[socket]._req.getIsChunked() == true)
			_clients[socket]._req.clearBody();
		if (_clients[socket].isBufferEmpty() == true) // means no error was raised while downloading files
		{
			if (_clients[socket]._req.getExpect() && _clients[socket]._req.getPostType() != -1)
				return;
			_clients[socket]._req.setExpect(0);
			_clients[socket].clearResponse();
			res.setStatusCode(204);
			_clients[socket].addToBuffer(res.createResponse());
		}
		return;
	}
	if (_clients[socket].requestIsOver())
	{
		_clients[socket].clearResponse();
		_clients[socket]._req.setExpect(0);
		res.setStatusCode(204);
		_clients[socket].addToBuffer(res.createResponse());
	}
}

void Server::executeDelete(int socket)
{
	Response res;
	std::string file;

	file = "." + _clients[socket]._req.getRequestUri();
	if (std::remove(file.c_str()))
		res.setStatusCode(_clients[socket].checkErrorConfig(403));
	else
		res.setStatusCode(200);
	_clients[socket].addToBuffer(res.createResponse());
}

void Server::printClientResponse(int socket)
{
	std::cout << "---\n\nResponse:\033[1;36m\n";
	_clients[socket].printBuffer();
	std::cout << "\033[0m------------\n\n"
			  << std::endl;
}

void Server::eraseClient(int client) // TODO : to implement in ClientHandler
{
	_clients[client].clean();
	_clients.erase(client);
}

std::ostream &operator<<(std::ostream &os, Server const &rh)
{
	os << "This is this object for server listening on port n." << rh.getPort() << std::endl;
	os << "Through socket n." << rh.getServerSocket() << std::endl;
	os << "With " << rh.getServerConfigs().size() << " config(s)" << std::endl;
	return (os);
}
