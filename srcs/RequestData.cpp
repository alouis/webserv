/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   RequestData.cpp                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 18:40:57 by clucien           #+#    #+#             */
/*   Updated: 2021/12/18 10:28:49 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "RequestData.hpp"

RequestData::RequestData( void )
{
	this->clear();
}

RequestData::RequestData( const RequestData &other )
{
	if ( this != &other )
		*this = other;
}

RequestData::~RequestData( void ) { }

RequestData				&RequestData::operator=( const RequestData &other )
{
	this->_method = other._method;
	this->_requestUri = other._requestUri;
	this->_httpVersion = other._httpVersion;
	this->_headers = other._headers;
	this->_messageBody = other._messageBody;

	return *this;
}

void				RequestData::clear( void )
{
	_requestRaw.clear();
	_method.clear();
	_requestUri.clear();
	_httpVersion.clear();
	_headers.clear();
	_headers.insert( std::make_pair( "", "" ) );
	_messageBody.clear();
	_isChunked = false;
	_isFirstChunk = true;
	_isCropped = false;
	_postType = 0;
	_bodyLength = 0;
	_statusCode = 200;
	_erasedR = 0;
	_headerLines = 0;
	_boundary.clear();
	_postElements.clear();
	_totalBytesRead = 0;
	_uriChecked = false;
	_permissionsChecked = false;
	_dirListing = false;
}

void				RequestData::clearBody( void )
{
	_messageBody.clear();
	_bodyLength = 0;
	_isFirstChunk = true; // ?
}

/**
 * parseRequest()
 * Parser router, return status code of the request.
 */
int		RequestData::parseRequest( uchar_vector requestRaw )
{
	int	statusBefore = _statusCode;

	// CPRINTL( "Request received ! (" << requestRaw.size() << ") isCropped: " << _isCropped )
	/* only keep read data, remove \r and keep score of it */
	prepareRequest( &requestRaw ); // keep read data must be deprec
	// CPRINTL( "New request, readable: " << requestRaw.size() )
	// for ( uchar_vector::iterator begin = requestRaw.begin(); begin != requestRaw.end(); ++begin )
	// 		std::cerr << *begin;

	if ( !getIsCropped() && !getIsChunked() )
		_statusCode = parseHead( &requestRaw );

	// CPRINTL("PARSING NEW REQUEST (isCropped: " << _isCropped << ") == BODY ==\n" << requestRaw)

	if ( _statusCode == 200 )
	{
		/* Handle pre chunk */
		if ( getIsChunked() )
			parseChunkedBody( &requestRaw );
		/* Handle pre no chunk/cropped */
		else
		{
			for ( uchar_vector::iterator begin = requestRaw.begin(); begin != requestRaw.end(); ++begin )
				_messageBody.push_back( *begin );
			_bodyLength += _messageBody.size() + _erasedR - _headerLines; // to handle /r diff
		}
		_statusCode = parseBody( _messageBody );
	}

	_statusCode = ( statusBefore != 200 && statusBefore != 204 ? statusBefore : _statusCode );
	// CPRINTL( "Leaving request with status: " << _statusCode << ", isCropped: " << _isCropped )
	return ( _statusCode == 200 ? 1 : 0 );
}

void	RequestData::handlePostMethod( void )
{
	/* Set boundary */
	std::string	contentType = getContentType();
	if ( contentType.find( "multipart/form-data" ) != std::string::npos )
	{
		_postType = MULTIPART;

		size_t	pos = contentType.find( "boundary=" );
		if ( pos != std::string::npos )
			_boundary = contentType.substr( pos + 9 );
	}
	else if ( contentType.find( "application/x-www-form-urlencoded" ) != std::string::npos )
		_postType = URLENCODED;
}

int		RequestData::checkHead( void )
{
	// check for unallowed methods
	if ( _method != "GET" && _method != "POST" && _method != "DELETE" )
		return 405;
	/* Set isChunk */
	checkIfRequestIsChunked();
	/* Handle POST */
	if ( _method == "POST" )
		handlePostMethod();
	return 200;
}

int		RequestData::parseHead( uchar_vector *reqRaw )
{
	uchar_vector::iterator	posIt;

	// reset line counter
	_headerLines = 0;
	// save the request as _requestRaw
	_requestRaw = *reqRaw;

	posIt = reqRaw->begin() + ucharFind( *reqRaw, ' ' );
	if ( posIt == reqRaw->end() )
		return 400;
	_method = ucharSubstr( reqRaw->begin(), posIt );
	reqRaw->erase( reqRaw->begin(), posIt + 1 );

	posIt = reqRaw->begin() + ucharFind( *reqRaw, ' ' );
	if ( posIt == reqRaw->end() )
		return 400;
	_requestUri = ucharSubstr( reqRaw->begin(), posIt );
	reqRaw->erase( reqRaw->begin(), posIt + 1 );

	posIt = reqRaw->begin() + ucharFind( *reqRaw, '\n' );
	++_headerLines;
	if ( posIt == reqRaw->end() )
		return 400;
	_httpVersion = ucharSubstr( reqRaw->begin(), posIt );
	reqRaw->erase( reqRaw->begin(), posIt + 1 );

	_headers.clear();
	_headers.insert( std::make_pair( "", "" ) );
	while ( reqRaw->size() && reqRaw->at( 0 ) != '\n' )
	{
		double_string_pair	val;
		posIt = reqRaw->begin() + ucharFind( *reqRaw, ':' );
		if ( posIt == reqRaw->end() )
			return 400;
		val.first = ucharSubstr( reqRaw->begin(), posIt );
		reqRaw->erase( reqRaw->begin(), posIt + 2 ); // + 2 is for the ": " (with the space after)

		posIt = reqRaw->begin() + ucharFind( *reqRaw, '\n' );
		if ( posIt == reqRaw->end() )
			return 400;
		_headerLines++;
		val.second = ucharSubstr( reqRaw->begin(), posIt );
		reqRaw->erase( reqRaw->begin(), posIt + 1 );
		_headers.insert( val );
	}

	if ( reqRaw->size() )
		reqRaw->erase( reqRaw->begin(), reqRaw->begin() + 1 ); // empty \n
	return checkHead();
}

bool	RequestData::checkIfRequestIsChunked( void )
{
	if ( getIsChunked() == true )
		return true;
	if ( _headers.find( "Transfer-Encoding" ) != _headers.end() )
	{
		std::pair<double_string_multimap::const_iterator, double_string_multimap::const_iterator>	headerLines;

		headerLines = getTransferEncoding();
		for ( double_string_multimap::const_iterator tE = headerLines.first; tE != headerLines.second; tE++ )
		{
			std::cout <<  tE->second.compare("chunked") << std::endl;
			if (tE->second.compare("chunked") == 0)
			{
				_isChunked = true;
				return true;
			}
		}
	}
	return false;
}

void	RequestData::parsePostMultipart( uchar_vector body )
{
	std::vector< t_postElement > tmpPost = _postElements;
	size_t	boundaryLength = _boundary.length();
	/* Remove the opening boundary */
	if ( ucharFind( body, _boundary ) == 2 )
		body.erase( body.begin(), body.begin() + boundaryLength + 2 + 1 ); // +2 for the -- before the boundary & +1 for \n

	// CPRINTL( "NEW BODY: ")
	// for ( uchar_vector::iterator begin = body.begin(); begin != body.end(); ++begin )
	// 	std::cerr << *begin;

	while ( body.size() )
	{
		/* Define the state of the request */
		size_t		bounderPos = ucharFind( body, _boundary );

		/* Check current state and substr body to only keep post element */
		uchar_vector	elemStr;
		if ( bounderPos + 1 == body.size() )
		{
			if ( _multipartState == BEGIN_OF_DATA_CROPPED || _multipartState == DATA_CROPPED )
				_multipartState = DATA_CROPPED;
			else
				_multipartState = BEGIN_OF_DATA_CROPPED;
			/* Copy in elemStr */
			elemStr = body;

			body.clear();
		}
		else
		{
			if ( _multipartState == DATA_CROPPED )
				_multipartState = END_OF_DATA_CROPPED;
			else
				_multipartState = DATA_COMPLETE;
			/* Insert in elemStr */
			elemStr.assign( body.begin(), body.begin() + bounderPos - 2 - 1 ); // -2 for --, -1 for \n

			uchar_vector::iterator	endOfElement = body.begin() + bounderPos + boundaryLength + 1;
			body.erase( body.begin(), endOfElement ); // +1 for \n
		}

		// CPRINTL( "NEW ELEMENT: ")
		// for ( uchar_vector::iterator begin = elemStr.begin(); begin != elemStr.end(); ++begin )
		// 	std::cerr << *begin;

		/* If request data is not being cropped */
		if ( _multipartState == DATA_COMPLETE || _multipartState == BEGIN_OF_DATA_CROPPED )
		{
			size_t	doubleBsPos = ucharFind( elemStr, "\n\n" );
			size_t	namePos = ucharFind( elemStr, "name=" );
			size_t	filenamePos = ucharFind( elemStr, "filename=" );

			/* NAME */
			std::string	name = "";
			if ( namePos + 1 != elemStr.size() )
				name = ucharSubstr( elemStr.begin() + namePos + 5 + 1, elemStr.begin() + ucharFind( elemStr, '\"', namePos + 5 + 1 ) ); // +1 for opening quotes
			/* FILENAME */
			std::string	filename = "";
			if ( filenamePos + 1 != elemStr.size() )
				filename = ucharSubstr( elemStr.begin() + filenamePos + 9 + 1, elemStr.begin() + ucharFind( elemStr, '\"', filenamePos + 9 + 1 ) ); // +1 for opening quotes
			/* HANDLE \n\n (mandatory) */
			if ( doubleBsPos + 1 != elemStr.size() )
				elemStr.erase( elemStr.begin(), elemStr.begin() + doubleBsPos + 2 );

			t_postElement	element;
			element.name = ( filename.length() ? filename : name );
			element.data = elemStr;
			if ( element.data.size() && element.name.size() )
			{
				CPRINTL( "NEW ENTRY: " << element.name << " - " << element.data.size() )
				// for ( uchar_vector::iterator begin = element.data.begin(); begin != element.data.end(); ++begin )
				// 	std::cerr << *begin;
				// std::cerr << '\n';
				tmpPost.push_back( element );
			}
		}
		/* If request data is being cropped */ // appears to be useless
		else if ( _multipartState == DATA_CROPPED || _multipartState == END_OF_DATA_CROPPED )
		{
			// for ( uchar_vector::iterator begin = tmpPost.back().data.begin(); begin != tmpPost.back().data.end(); ++begin )
			// 	std::cerr << *begin;
			// std::cerr << '\n';
			tmpPost.back().data.insert( tmpPost.back().data.end(), elemStr.begin(), elemStr.end() );
			CPRINTL( "ENTRY APPEND: " << tmpPost.back().name << " - " << tmpPost.back().data.size() << " (+" << elemStr.size() << ")" )
		}
		/* Handle end of multipart */
		if ( body.size() > 1 && body.at( 0 ) == '-' && body.at( 1 ) == '\n' )
		{
			setIsCropped( false );
			setIsChunked( false );
			_postType = -1; // because multipart is over this "round"
			body.clear();
		}
	}
	_postElements.clear();
	_postElements = tmpPost;
}

void	RequestData::parsePostUrlencoded( uchar_vector body )
{
	(void)body;
}

int		RequestData::checkBody( void )
{
	// check for message body size (empty message = 1) and set _isCropped accordingly
	if ( getContentLength() > _bodyLength )
	{
		if ( _postType != -1 )
			setIsCropped( true );
	}
	// end of a multipart cropped post must be a boundary (pb when no boundary)
	else if ( _postType != MULTIPART )
		setIsCropped( false );
	return 200;
}

int		RequestData::parseBody( uchar_vector messageBody )
{
	if ( _postType == MULTIPART )
		parsePostMultipart( messageBody ); // adress because of opti and time exec
	/* Parsing done in CGI part */
	// else if ( _postType == URLENCODED )
	// 	parsePostUrlencoded( messageBody );

	return checkBody();
}

int		RequestData::parseChunkedBody( uchar_vector *reqBody )
{
	uchar_vector::iterator	endOfChunk;
	size_t					chunkSize = 1;

	// CPRINTL( "Entering parsing loops:" )
	// uchar_vector::const_iterator it;
	// for ( it = reqBody->begin(); it != reqBody->end(); it++ )
	// 	std::cerr << *it;

	while ( reqBody->size() > 1 && chunkSize )
	{
		std::string					hexa;
		std::string					extension;
		uchar_vector::iterator		pos = reqBody->begin() + ucharFind( *reqBody, '\n' );
		uchar_vector::iterator		posBr = reqBody->begin() + ucharFind( *reqBody, ';' );

		hexa = ucharSubstr( reqBody->begin(), pos );
		// CPRINTL( "Reading hexa code: " << hexa << ", size of reqBody: " << reqBody->size() )

		reqBody->erase( reqBody->begin(), pos + 1 );
		if ( posBr > pos || posBr == reqBody->end() ) // check for chunk extension
			extension = "";
		else if ( posBr < pos ) // handle said extension (will be ignored)
		{
			hexa = ucharSubstr( reqBody->begin(), posBr );
			extension = ucharSubstr( posBr + 1, pos );
			reqBody->erase( reqBody->begin(), pos + 1 );
		}

		chunkSize = ( int )strtol( hexa.c_str(), 0, 16 );
		/* If first or last chunk */
		if ( _isFirstChunk )
		{
			chunkSize -= hexa.length();
			_isFirstChunk = false;
		}
		if ( chunkSize == reqBody->size() - 2 )
			chunkSize -= hexa.length() - 2;
		else if ( chunkSize >= reqBody->size() )
			chunkSize = reqBody->size() - 1;
		endOfChunk = reqBody->begin() + chunkSize;

		// append to messageBody
		_messageBody.insert( _messageBody.end(), reqBody->begin(), endOfChunk );

		reqBody->erase( reqBody->begin(), endOfChunk + 1 );
		_bodyLength += ( unsigned long )chunkSize;
	}

	if ( chunkSize == 0 )
		setIsChunked( false );

	// CPRINTL( "Buffer after chunk parsing:" )
	// uchar_vector::const_iterator it;
	// for ( it = _messageBody.begin(); it != _messageBody.end(); it++ )
	// 	std::cerr << *it;

	return 200;
}

std::string const			&RequestData::headersSecureGet( std::string const key ) const
{
	if ( _headers.find( key ) != _headers.end() )
		return _headers.find( key )->second;
	else
		return _headers.find( "" )->second;
}

void		RequestData::prepareRequest( uchar_vector *requestRaw )
{
	/* remove \r */
	_erasedR = 0;
	for ( uchar_vector::iterator begin = requestRaw->begin(); begin != requestRaw->end(); ++begin )
	{
		if ( *begin == '\r' && *( begin + 1 ) == '\n' )
		{
			_erasedR++;
			requestRaw->erase( begin );
		}
	}
}

/**
 * ucharFind()
 *
 * @param vec vector to parse
 * @param charac char to find
 * @return pos size_t of the first occurence of charac in vec. vec.end() if charac isn't found.
 */
size_t		RequestData::ucharFind( uchar_vector const &vec, unsigned char const &charac, size_t from )
{
	size_t	pos = from;

	for ( uchar_vector::const_iterator begin = vec.begin() + from; *begin != charac && begin != vec.end(); ++begin )
		++pos;
	return pos;
}

size_t		RequestData::ucharFind( uchar_vector const &vec, std::string const &str, size_t from )
{
	size_t	pos = from;
	bool	found = false;

	for ( uchar_vector::const_iterator begin = vec.begin() + from; begin != vec.end() && !found; ++begin )
	{
		for ( size_t i = 0; *( begin + i ) == str[ i ] && !found; ++i )
		{
			if ( i + 1 == str.length() )
				found = true;
		}
		++pos;
	}
	return pos - 1;
}

/**
 * ucharSubstr()
 *
 * @param begin first character to retrun
 * @param end last character to return
 * @return a string strating from pos to pos + len. If !end or len == -1, go from pos to vec.end()
 */
std::string	const			RequestData::ucharSubstr( uchar_vector::const_iterator begin, uchar_vector::const_iterator end ) const
{
	std::string				newStr;

	while ( begin != end )
	{
		newStr.append( 1, *begin );
		++begin;
	}
	return newStr;
}

/**
 * Getters
 */

uchar_vector const		&RequestData::getRequestRaw( void ) const
{
	return _requestRaw;
}

std::string const		&RequestData::getMethod( void ) const
{
	return _method;
}

std::string	const		&RequestData::getRequestUri( void ) const
{
	return _requestUri;
}

std::string	const 		RequestData::getUriPath( void ) const
{
	std::string	path = _requestUri.substr( 0, _requestUri.find_last_of( '/' ) );
	if ( path.length() )
		return path;
	else if ( _requestUri.length() )
		return "";
	//TODO: beware => same return on error
	else
		return "";
}

std::string	const 		RequestData::getUriFile( void ) const
{
	std::string	file = _requestUri.substr( _requestUri.find_last_of( '/' ) );
	if ( file.length() )
		return file;
	else if ( _requestUri.length() )
		return "/";
	else
		return "";
}

std::string	const		&RequestData::getHttpVersion( void ) const
{
	return _httpVersion;
}

double_string_multimap	&RequestData::getHeaders( void )
{
	return _headers;
}

uchar_vector const		&RequestData::getMessageBody( void ) const
{
	return _messageBody;
}

std::string const		RequestData::getStringMessageBody( void ) const
{
	return ucharSubstr( _messageBody.begin(), _messageBody.end() );
}

bool					RequestData::getIsChunked( void ) const
{
	return _isChunked;
}

bool					RequestData::getIsCropped( void ) const
{
	return _isCropped;
}

int						RequestData::getStatusCode() const
{
	return _statusCode;
}

unsigned long						RequestData::getBodyLength() const
{
	return _bodyLength;
}

unsigned long			RequestData::getTotalBytesRead() const
{
	return _totalBytesRead;
}

/**
 * Header getters
 */

unsigned long			RequestData::getContentLength( void ) const
{
	return std::atoll( headersSecureGet( "Content-Length" ).c_str() );
}

std::string const			&RequestData::getContentType( void ) const
{
	return headersSecureGet( "Content-Type" );
}

std::pair<double_string_multimap::const_iterator, double_string_multimap::const_iterator>	RequestData::getTransferEncoding( void ) const
{
	if ( _headers.find( "Transfer-Encoding" ) != _headers.end() )
		return _headers.equal_range( "Transfer-Encoding" );
	else
		return _headers.equal_range( "" );
}

int							RequestData::getExpect( void ) const
{
	if (_headers.find( "Expect" ) != _headers.end())
		return std::atoi( headersSecureGet( "Expect" ).c_str() );
	return 0;
}

std::string const 			&RequestData::getHost( void ) const
{
	return headersSecureGet( "Host" );
}

/**
 * Post Specs. getters
 */
std::vector< t_postElement >	&RequestData::getPostElements( void )
{
	return _postElements;
}


int								RequestData::getPostType( void ) const
{
	return _postType;
}

bool							RequestData::isUriChecked(void) const
{
	return _uriChecked;
}

Routes const& 					RequestData::getSelectedRoute(void) const
{
	return _selectedRoute;
}


bool							RequestData::arePermissionsChecked(void) const
{
	return _permissionsChecked;
}

bool							RequestData::getDirListing(void) const
{
	return _dirListing;
}

void							RequestData::clearLastDataPostElem( void )
{
	std::vector< t_postElement >	tmpElements;
	t_postElement					tmpPost;

	tmpPost = _postElements.back();
	tmpPost.data.clear();
	tmpElements.push_back( tmpPost );
	_postElements.clear();
	_postElements = tmpElements;
}

void							RequestData::clearTotalBytesRead( void )
{
	_totalBytesRead = 0;
}
/**
 * Setters
 */

void					RequestData::setUriPath( std::string newPath )
{
	_requestUri.replace( 0, _requestUri.find_last_of( '/' ), newPath );
}

void 					RequestData::setUriFile(std::string const& newFile)
{
	_requestUri.replace(_requestUri.find_last_of( '/' ), _requestUri.size(), newFile);
}

void					RequestData::setIsChunked( bool value )
{
	_isChunked = value;
}

void					RequestData::setIsCropped( bool value )
{
	_isCropped = value;
}

void					RequestData::setStatusCode(int statusCode)
{
	_statusCode = statusCode;
}

void					RequestData::setUriChecked(bool checked)
{
	_uriChecked = checked;
}

void					RequestData::setSelectedRoute(Routes const& route)
{
	_selectedRoute = route;
}

void					RequestData::setPermissionsChecked(bool checked)
{
	_permissionsChecked = checked;
}
void					RequestData::addToBytesRead( unsigned long bytesRead )
{
	if (bytesRead > 0)
		_totalBytesRead += bytesRead;
}


void					RequestData::setExpect(int value)
{
	if (_headers.find( "Expect" ) != _headers.end())
		_headers.find( "Expect" )->second = std::to_string( value );
}

void 					RequestData::setDirListing(bool value)
{
	_dirListing = value;
}

/**
 * Debug tools
 */

void				RequestData::printRequest( void )
{
	CPRINTL( "method: '" << _method << "'" )
	CPRINTL( "request URI: '" << _requestUri << "'" )
	CPRINTL( "HTTP Version: '" << _httpVersion << "'" )
	CPRINTL("Headers:")
	for ( double_string_multimap::iterator begin = _headers.begin(); begin != _headers.end(); ++begin )
		CPRINTL( "'" << begin->first << "': '" << begin->second << "'"  )
	// if (_messageBody.size() < 100)
		// CPRINTL( "\nMessage Body:\n'" << _messageBody << "'" )
	// else
	// 	std::cout << "Message body 's size greater than 100 bytes \n";
}
