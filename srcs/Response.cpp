/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   Response.cpp                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: alouis <alouis@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/27 11:15:18 by alouis            #+#    #+#             */
/*   Updated: 2021/09/29 17:39:40 by alouis           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "Response.hpp"

Response::Response() : _statusCode(0), _statusLine("HTTP/1.1 "),
					   _date("Date: "),
					   _size(0)
{
	time_t rawTime;

	time(&rawTime);
	_date.append(ctime(&rawTime));
	_body.clear();
}

Response::Response(const int newStatus) : _statusCode(newStatus), _statusLine("HTTP/1.1 "),
										  _date("Date: "),
										  _size(0)
{
	time_t rawTime;

	time(&rawTime);
	_date.append(ctime(&rawTime));
	_statusLine.append(createStatusMsg(newStatus));
	_body.clear();
}

Response::Response(const Response &src)
{
	*this = src;
}

Response &Response::operator=(const Response &src)
{
	(void)src;
	return *this;
}

Response::~Response() {}

ssize_t Response::getResponseSize() const { return _size; }

size_t Response::getBodySize() const {
	return _body.size();
}

std::string const Response::createStatusMsg(const int statusCode)
{
	if (statusCode == 100)
		return ("100 Continue\n");
	if (statusCode == 200)
		return ("200 OK\n");
	if (statusCode == 204)
		return ("204 No Content\n");
	if (statusCode == 301)
		return ("301 Moved Permanently\n");
	if (statusCode == 400)
	{
		loadErrorPage(statusCode);
		return ("400 Bad Request\n");
	}
	if (statusCode == 403)
	{
		loadErrorPage(statusCode);
		return ("403 Forbidden\n");
	}
	if (statusCode == 404)
	{
		loadErrorPage(statusCode);
		return ("404 Not Found\n");
	}
	if (statusCode == 405)
	{
		loadErrorPage(statusCode);
		addExtraHeader("Allow: GET, POST, HEAD\r\n");
		return ("405 Method Not Allowed\n");
	}
	if (statusCode == 413)
	{
		loadErrorPage(statusCode);
		return ("413 Payload Too Large\n");
	}
	if (statusCode == 415)
	{
		loadErrorPage(statusCode);
		return ("415 Unsupported Media Type\n");
	}
	if (statusCode == 500)
	{
		loadErrorPage(statusCode);
		return ("500 Internal Server Error\n");
	}
	loadErrorPage(418);
	return ("418 I'm a teapot\n");
}

void Response::loadErrorPage(int statusCode)
{
	std::string buffer;

	if (statusCode == 418)
		buffer = "./error/" + std::to_string(statusCode) + ".jpg";
	else
		buffer = "./error/" + std::to_string(statusCode) + ".html";
	_file.open(buffer.c_str(), std::fstream::in);
	_body.clear();
	if (_file.is_open())
	{
		std::streampos fileSize;
		_file.seekg(0, std::ios::end);
		fileSize = _file.tellg();
		_file.seekg(0, std::ios::beg);

		std::vector<unsigned char> fileData(fileSize);
		_file.read((char *)&fileData[0], fileSize);
		_file.close();

		addToBody(fileData);
	}
	else
		std::cout << "\033[1;33mFor whatever reason template page for error " << statusCode << " wasn't available\033[0m\n";
}

void Response::setStatusCode(int newStatusCode)
{
	_statusLine.assign("HTTP/1.1 ");
	_statusCode = newStatusCode;
	_statusLine.append(createStatusMsg(_statusCode));
}

void Response::addToBody(std::string buff)
{
	_body.insert(_body.end(), buff.begin(), buff.end());
}

void Response::addToBody(std::vector<unsigned char> buff)
{
	_body.insert(_body.end(), buff.begin(), buff.end());
}

void  Response::addToBodyAsChunk(std::vector<unsigned char> content)
{
	std::stringstream stream;
	std::string separator("\r\n");

	stream << std::hex << content.size();
	std::string result(stream.str());
	_body.insert(_body.end(), result.begin(), result.end());
	_body.insert(_body.end(), separator.begin(), separator.end());
	_body.insert(_body.end(), content.begin(), content.end());
	_body.insert(_body.end(), separator.begin(), separator.end());
}

void Response::addExtraHeader(std::string str)
{
	for (std::string::iterator it = str.begin(); it != str.end(); it++)
		_extraHeader.insert(_extraHeader.end(), *it);
}
void Response::addExtraHeader(std::vector<unsigned char> buff)
{
	_extraHeader.insert(_extraHeader.end(), buff.begin(), buff.end());
}

void Response::setContentLenght(ssize_t lenght)
{
	_contentLength.assign("Content-Length: " + std::to_string(lenght) + "\n");
}

void Response::setContentType(std::string mediaType)
{
	if (mediaType.size())
		_contentType.assign("Content-Type: " + mediaType + "\n");
}

void Response::setTransferEncoding(std::string tEncoding)
{
	if (tEncoding.size())
		_transferEncoding.assign("Transfer-Encoding: " + tEncoding + "\n");
}

void Response::setLocation(std::string location)
{
	if (location.size())
		_location.assign("Location: " + location + "\n");
}

std::vector<unsigned char> Response::createResponse()
{
	std::vector<unsigned char> response;
	std::string separator("\r\n");

	response.insert(response.end(), _statusLine.begin(), _statusLine.end());
	if (!_statusCode)
		throw NotCompliantException();
	if (_date.size() > 6)
		response.insert(response.end(), _date.begin(), _date.end());
	if (_contentType.size())
		response.insert(response.end(), _contentType.begin(), _contentType.end());
	if (_transferEncoding.compare("Transfer-Encoding: chunked\n"))
	{
		setContentLenght(_body.size());
		response.insert(response.end(), _contentLength.begin(), _contentLength.end());
	}
	if (_transferEncoding.size())
		response.insert(response.end(), _transferEncoding.begin(), _transferEncoding.end());
	if (_location.size())
		response.insert(response.end(), _location.begin(), _location.end());
	if(_extraHeader.size())
		response.insert(response.end(), _extraHeader.begin(), _extraHeader.end());
	response.insert(response.end(), separator.begin(), separator.end());
	if (_body.size())
		response.insert(response.end(), _body.begin(), _body.end());
	return (response);
}

std::vector<unsigned char>	Response::createChunkBlock(std::vector<unsigned char> content)
{
	std::stringstream stream;
	std::string separator("\r\n");

	stream << std::hex << content.size();
	std::string result(stream.str());
	_body.clear();
	_body.insert(_body.end(), result.begin(), result.end());
	_body.insert(_body.end(), separator.begin(), separator.end());
	_body.insert(_body.end(), content.begin(), content.end());
	_body.insert(_body.end(), separator.begin(), separator.end());
	return _body;
}

std::string Response::formatDirListing(RequestData const& req, std::string const& initialUri)
{
	std::string dirPath = "." + req.getRequestUri();
	DIR* rep = NULL;
	std::string result;

	std::cout << "==============req: " << initialUri << std::endl;

    struct dirent* dir = NULL;
    rep = opendir(dirPath.c_str());
    if (rep == NULL)
		return ("Error in dir listing");

	if ((dir = readdir(rep))== NULL)
		return ("Nothing here");
	result.append("<html>");
	result.append(dir->d_name);
    while ((dir = readdir(rep))!= NULL)
	{
		result.append("<br/>\n");
		result.append("<a href=" + initialUri + dir->d_name + ">" + dir->d_name + "</a>");
	}
	result.append("</html>");
    if (closedir(rep) == -1)
        return ("Error in dir listing");
	return result;
}

std::ostream &operator<<(std::ostream &os, Response &rh)
{
	os << &rh.createResponse()[0] << std::endl;
	return os;
}
