/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ClientHandler.cpp                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/03 10:11:34 by alouis            #+#    #+#             */
/*   Updated: 2021/12/18 10:59:57 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ClientHandler.hpp"

ClientHandler::ClientHandler(fd_set *readSocketsTemp, fd_set *readSockets, fd_set *writeSocketsTemp, fd_set *writeSockets) : _readSocketsTemp(readSocketsTemp), _readSockets(readSockets),
																															 _writeSocketsTemp(writeSocketsTemp), _writeSockets(writeSockets),
																															 _socket(0), _config(NULL), _responseIsChunked(false)
{
}

ClientHandler::ClientHandler() : _readSocketsTemp(NULL), _readSockets(NULL),
								 _socket(0), _config(NULL), _responseIsChunked(false)
{
}
ClientHandler::ClientHandler(const ClientHandler &src)
{
	*this = src;
}

ClientHandler &ClientHandler::operator=(const ClientHandler &src)
{
	if (this != &src)
	{
		this->_socket = src._socket;
		this->_buffer = src._buffer;
		this->_config = src._config;
		this->_responseIsChunked = src._responseIsChunked;
		this->_readSocketsTemp = src._readSocketsTemp;
		this->_readSockets = src._readSockets;
		this->_writeSocketsTemp = src._writeSocketsTemp;
		this->_writeSockets = src._writeSockets;
	}
	return (*this);
}

ClientHandler::~ClientHandler() {}

void ClientHandler::clean()
{
	if (isCGIRunning())
	{
		close(_cgiHandler.getFd());
		FD_CLR(_cgiHandler.getFd(), _readSockets);
	}
	FD_CLR(_socket, _readSockets);
	FD_CLR(_socket, _writeSockets);
	close(_socket);
}

std::vector<unsigned char> &ClientHandler::getBuffer() { return (_buffer); }
bool ClientHandler::isResponseChunked() const { return (_responseIsChunked); }
int ClientHandler::getSocket() const { return (_socket); }
bool ClientHandler::isBufferEmpty() const { return !(_buffer.size()); }

bool ClientHandler::errorRaised() const
{
	if (_req.getStatusCode() != 200 && _req.getStatusCode() != 204 && _req.getStatusCode() != 100)
		return true;
	return false;
}

void ClientHandler::setSocket(int newSocket)
{
	_socket = newSocket;
}

void ClientHandler::setConfig(ServerConfig *newConfig)
{
	if (newConfig != _config)
		_config = newConfig;
}

int ClientHandler::checkPermissions()
{
	Response res;

	_req.setPermissionsChecked(true);
	for (std::vector<std::string>::const_iterator method = _req.getSelectedRoute().getHTTPMethodsAccepted().begin(); method != _req.getSelectedRoute().getHTTPMethodsAccepted().end(); method++)
	{
		if (*method == _req.getMethod())
			return (1);
	}
	res.setStatusCode(checkErrorConfig(405));
	addToBuffer(res.createResponse());
	return (0);
}

void ClientHandler::checkUri()
{
	Routes selectedRoute;
	if (_req.isUriChecked())
		return;
#ifdef PRINT
	std::cout << "\033[94mRequestUri before: " << _req.getRequestUri() << " - UriPath: " << _req.getUriPath() << "\033[0m\n";
#endif
	_initialUri = _req.getRequestUri();

	for (std::vector<Routes>::const_iterator route = _config->getRoutes().begin(); route != _config->getRoutes().end(); route++)
	{
		if (!route->getRoutePath().compare(_req.getUriPath()))
			_req.setSelectedRoute(*route);
	}

	if (_req.getSelectedRoute().getRoot().compare("") == 0)
	{
		for (std::vector<Routes>::const_iterator route = _config->getRoutes().begin(); route != _config->getRoutes().end(); route++)
		{
			if (!route->getRoutePath().compare("/"))
				_req.setSelectedRoute(*route);
		}
	}

	selectedRoute = _req.getSelectedRoute();

	if(selectedRoute.getRoutePath().compare("/") == 0)
		_req.setUriPath(selectedRoute.getRoot() + _req.getUriPath());
	else
		_req.setUriPath(selectedRoute.getRoot());

	if (_req.getUriFile().compare("/") == 0)
	{
		if (_req.getSelectedRoute().getAutoIndex() == true)
			_req.setUriFile("/" + selectedRoute.getDefaultFileIfDirectoryRequest());
		else
			_req.setDirListing(true);
	}
#ifdef PRINT
	std::cout << "\033[94mRequestUri after: " << _req.getRequestUri() << " - UriPath: " << _req.getUriPath() << "\033[0m\n";
#endif
	_req.setUriChecked(true);
	return;
}

int ClientHandler::openFile()
{
	Response res;

	// Dir Listing
	if (_req.getDirListing() == true)
	{
		std::string body = res.formatDirListing(_req, _initialUri);
		res.setStatusCode(200);
		res.addToBody(body);
		res.setContentLenght(body.size());
		res.setStatusCode(200);
		addToBuffer(res.createResponse());
		return (0);
	}
	// check for empty response for siege benchmark
	else if (_req.getRequestUri() == "/files1/empty.html")
	{
		res.setStatusCode(200);
		addToBuffer(res.createResponse());
		return (0);
	}
	// Uri == "" means bad request
	else if (_req.getRequestUri() == "")
	{
		res.setStatusCode(checkErrorConfig(400));
		addToBuffer(res.createResponse());
		return (0);
	}
	// redirection
	else if (!_req.getUriFile().compare(_req.getSelectedRoute().getHTTPRedirection().from))	//let's replace with  so every request for hello_world is redirected
	{
		res.setStatusCode(301);
		res.setLocation(_req.getSelectedRoute().getHTTPRedirection().to);
		addToBuffer(res.createResponse());
		return (0);
	}
	_file.open("." + _req.getRequestUri(), std::fstream::in | std::ios::binary);
	if (_file.is_open())
		return (1);
	res.setStatusCode(checkErrorConfig(404));
	addToBuffer(res.createResponse());
	return (0);
}

void ClientHandler::readFile()
{
	Response res;
	std::streampos fileSize;

	_file.seekg(0, std::ios::end);
	fileSize = _file.tellg();
	_positionInFile = fileSize;
	_file.seekg(0, std::ios::beg);
	if (fileSize > 260000)
		chunkResponse();
	else
	{
		std::vector<unsigned char> fileData(fileSize);

		_file.read((char *)&fileData[0], fileSize);
		if (!_file)
		{
			errorReadingFile();
			return;
		}
		closeFile();
		res.addToBody(fileData);
		res.setContentLenght(fileSize);
		res.setContentType(_req.getContentType());
		res.setStatusCode(200);
		addToBuffer(res.createResponse());
	}
}
int ClientHandler::chunkResponse()
{
	Response res;
	std::vector<unsigned char> fileData(260000);

	_responseIsChunked = true;
	_file.read((char *)&fileData[0], 260000);
	if (!_file)
		return errorReadingFile();
	res.addToBodyAsChunk(fileData);
	res.setTransferEncoding("chunked");
	res.setContentType(_req.getContentType());
	res.setStatusCode(200);
	addToBuffer(res.createResponse());
	_positionInFile = _file.tellg();
	return (1);
}

int ClientHandler::readFileInChunk()
{
	Response res;
	std::streampos fileSize;

	_file.seekg(0, std::ios::end);
	fileSize = _file.tellg() - _positionInFile;
	_file.seekg(_positionInFile);
	if (fileSize > 260000)
	{
		std::vector<unsigned char> fileData(260000);

		_file.read((char *)&fileData[0], 260000);
		if (!_file)
			return errorReadingFile();
		addToBuffer(res.createChunkBlock(fileData));
		_positionInFile = _file.tellg();
		return (1);
	}
	else
	{
		std::vector<unsigned char> fileData(fileSize);

		_file.read((char *)&fileData[0], fileSize);
		if (!_file)
			return errorReadingFile();
		closeFile();
		addToBuffer(res.createChunkBlock(fileData));
		fileData.clear();
		addToBuffer(res.createChunkBlock(fileData));
		_responseIsChunked = false;
		return (1);
	}
}

bool ClientHandler::isCGIRunning()
{
	return _cgiHandler.isRunning();
}

void ClientHandler::transferCGIResponseToBuffer()
{
	Response res;

	res.setStatusCode(200);
	res.setContentLenght(_cgiHandler.getContentLength());
	res.addExtraHeader(_cgiHandler.extractHeader());
	res.addToBody(_cgiHandler.extractBody());
	addToBuffer(res.createResponse());
}

bool ClientHandler::handleCGI()
{
	if (!isCGIRunning())
	{
		if (!CGIHandler::isCGISupportNeeded(_req.getRequestUri(), _req.getSelectedRoute().getCGIExtensions()))
			return false;

		std::vector<std::string> argv;
		_cgiHandler.runCGIRequest(_req, _readSockets, _writeSockets);
	}
	else
	{
		if (_req.getMethod() == "POST")
		{
			uchar_vector data = _req.getMessageBody();
			if (_req.getIsCropped() || data.empty() != true)
			{
				_cgiHandler.writeData(data, _writeSocketsTemp);
				_req.clearBody();
			}
			else
			{
				_cgiHandler.closeWritePipe(_writeSockets);
			}
		}
		_cgiHandler.readData(_readSocketsTemp, _readSockets);
		if (!isCGIRunning())
			transferCGIResponseToBuffer();
	}
	return true;
}

int ClientHandler::loadDataInFile(t_postElement post)
{
	int ret;
	std::ofstream _file;
	std::string fileUploadDirectory = "." + _req.getSelectedRoute().getFileUploadDirectory() + "/";

	ret = 0;
	// if (post.data.size() < 260000)
	// 	std::cout << "POST ELEM: name=" << post.name << " & data=" << post.data << std::endl;
	_file.open(fileUploadDirectory + post.name, std::ofstream::binary | std::ios_base::app);
	if (_file.fail())
	{
		std::cout << "\033[1;31mOpening file " << post.name << " \033[0m\n";
		_file.open(fileUploadDirectory + post.name, std::ofstream::binary | std::ios_base::out | std::ios_base::app);
	}
	if (_file.is_open())
	{
		for (uchar_vector::const_iterator begin = post.data.begin(); begin != post.data.end(); ++begin)
		{
			_file.write(reinterpret_cast<char const *>(&(*begin)), sizeof(unsigned char));
		}
		if (_file.fail() == false)
			ret = 1;
	}
	_file.close();
	return (ret);
}

int ClientHandler::errorReadingFile()
{
	Response res;

	closeFile();
	res.setStatusCode(checkErrorConfig(403));
	addToBuffer(res.createResponse());
	_responseIsChunked = false;
	return (0);
}

void ClientHandler::addToBuffer(std::vector<unsigned char> newBuffer)
{
	_buffer.insert(_buffer.end(), newBuffer.begin(), newBuffer.end());
}

int	ClientHandler::checkErrorConfig(int error)
{
	for (std::vector<int>::const_iterator it = _req.getSelectedRoute().getErrorPage().codes.begin(); it != _req.getSelectedRoute().getErrorPage().codes.end(); it++)
	{
		if (*it == error)
		{
			std::size_t	pos;
			std::string newErrorPage;
			int			newErrorCode;

			newErrorPage = _req.getSelectedRoute().getErrorPage().path;
			if ((pos = newErrorPage.find_last_of("/")) != std::string::npos)
				newErrorPage = newErrorPage.substr(pos + 1, newErrorPage.size());
			if ((pos = newErrorPage.find_first_of(".")) != std::string::npos)
				newErrorPage = newErrorPage.substr(0, pos);
			newErrorCode = atoi(newErrorPage.c_str());
			if (newErrorCode != 0)
				return (newErrorCode);
		}
	}
	return (error);
}

void ClientHandler::printBuffer() const
{
	std::vector<unsigned char>::const_iterator it;

	std::cout << "\033[0;32mBuffer is now:\n";
	for (it = _buffer.begin(); it != _buffer.end() && (it - _buffer.begin()) < MAX_PRINT; it++)
		std::cout << *it;

	if (_buffer.end() != it)
		std::cout << "...\n\n[Output truncated]";
	std::cout << std::endl;
	std::cout << "\033[0m\n";
}

unsigned long ClientHandler::getClientMaxBodySize() const
{
	return (_config->getClientMaxBodySize());
}

void ClientHandler::clearRequest()
{
#ifdef PRINT
	std::cout << "Request cleared\n";
#endif
	_req.clear();
}

void ClientHandler::clearResponse()
{
	_buffer.clear();
}

void ClientHandler::closeFile()
{
	if (_file.is_open())
		_file.close();
}

bool ClientHandler::requestIsOver() const
{
	if (_req.getIsCropped() == true || _req.getIsChunked() == true)
		return false;
	return true;
}

void ClientHandler::createExpectedResponse()
{
	Response res;

	if (_req.getExpect())
	{
		res.setStatusCode(_req.getExpect());
		addToBuffer(res.createResponse());
	}
}

std::ostream &operator<<(std::ostream &os, ClientHandler &rh)
{
	os << "This is this object for handling client listening on socket n." << rh.getSocket() << std::endl;
	os << "With body being: \"";
	for (std::vector<unsigned char>::iterator it = rh.getBuffer().begin(); it != rh.getBuffer().end(); it++)
		os << *it;
	os << "\"\n";
	return (os);
}
