/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ServerConfig.cpp                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/26 11:37:18 by mroux             #+#    #+#             */
/*   Updated: 2021/12/17 20:52:44 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ServerConfig.hpp"

ServerConfig::ServerConfig() : _default(false), _port(SERVER_PORT), _host("127.0.0.1"), _serverNames(), _root("/"), _clientMaxBodySize(100000),
							   _defaultErrorPage(), _routes()
{
}

ServerConfig::ServerConfig(
	bool def, int port, std::string host, std::vector<std::string> serverNames, std::string root,
	int clientMaxBodySize, errorPage defaultErrorPage, std::vector<Routes> routes) : _default(def), _port(port), _host(host), _serverNames(serverNames), _root(root), _clientMaxBodySize(clientMaxBodySize),
																					 _defaultErrorPage(defaultErrorPage), _routes(routes)
{
}

ServerConfig::~ServerConfig()
{
}

ServerConfig::ServerConfig(ServerConfig const &cl)
{
	operator=(cl);
}

ServerConfig &ServerConfig::operator=(ServerConfig const &cl)
{
	_default = cl._default;
	_port = cl._port;
	_host = cl._host;
	_serverNames = cl._serverNames;
	_root = cl._root;
	_clientMaxBodySize = cl._clientMaxBodySize;
	_defaultErrorPage = cl._defaultErrorPage;
	_routes = cl._routes;
	return (*this);
}

int ServerConfig::getPort() const { return _port; }
in_port_t ServerConfig::getPortInNetworkByteOrder() const { return htons(_port); };
std::string const &ServerConfig::getHost() const { return _host; };
in_addr_t ServerConfig::getHostInNetworkByteOrder() const throw(BadHostFormatException)
{
	in_addr_t addr;
	if (inet_pton(AF_INET, _host.c_str(), &addr) != 1)
		throw BadHostFormatException();
	return addr;
}
std::vector<std::string> const &ServerConfig::getServerNames() const { return _serverNames; }
unsigned long ServerConfig::getClientMaxBodySize() const { return _clientMaxBodySize; }
std::string const &ServerConfig::getRoot() const { return _root; }
bool ServerConfig::getDefault() const { return _default; }
errorPage const &ServerConfig::getDefaultErrorPage() const { return _defaultErrorPage; }
std::vector<Routes> const &ServerConfig::getRoutes() const { return _routes; }

std::ostream &operator<<(std::ostream &stream, ServerConfig const &cl)
{
	stream << "========== Server Config ========== " << std::endl;
	stream << "Port: " << cl.getPort() << std::endl;
	stream << "Host: " << cl.getHost() << std::endl;
	stream << "Default: " << cl.getDefault() << std::endl;
	stream << "ServerNames:";
	for (size_t i = 0; i < cl.getServerNames().size(); i++)
		stream << " " << cl.getServerNames()[i];
	stream << std::endl;
	stream << "ClientMaxBodySize: " << cl.getClientMaxBodySize() << std::endl;
	stream << "Root: " << cl.getRoot() << std::endl;
	stream << "Default error page: " << cl.getDefaultErrorPage() << std::endl;
	for (std::vector<Routes>::const_iterator it = cl.getRoutes().cbegin(); it != cl.getRoutes().end(); it++)
		stream << *it << std::endl;
	stream << "======================================" << std::endl;
	return (stream);
}
