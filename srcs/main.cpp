/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.cpp                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: mroux <mroux@student.42.fr>                +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/09/27 14:19:14 by alouis            #+#    #+#             */
/*   Updated: 2021/12/17 11:15:08 by mroux            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "tcpServer.hpp"
#include "ServersMonitor.hpp"
#include "ServerConfig.hpp"
#include "ServerConfigParser.hpp"
#include "CGIHandler.hpp"

#include <errno.h>

bool on;
char **env;

void signalHandler(int signum)
{
	std::cout << "CTRL+C was hit (signal = " << signum << "), closing listening sockets...\n";
	on = false;
}

int main(int argc, char **argv, char **envp)
{
	ServerConfigParser serverConfigParser;
	ServerConfigs serverConfigs;

	env = envp;
	on = true;
	signal(SIGINT, signalHandler);
	signal(SIGTERM, signalHandler);
	signal(SIGKILL, signalHandler);
	try
	{
		serverConfigParser = checkArguments(argc, argv) ? ServerConfigParser(argv[1]) : ServerConfigParser();
		serverConfigs = serverConfigParser.buildServerConfigs();
	}
	catch (std::exception &e)
	{
		std::cout << e.what() << std::endl;
		exit(-1);
	}
	try
	{
		ServersMonitor serverMonitor(serverConfigs);

		while (on)
		{
			serverMonitor.run();
		}
	}
	catch (const std::exception &e)
	{
		exit(-1);
	}
	return (0);
}
