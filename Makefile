NAME		=	webserv

CC			=	clang++

SRCS_DIR 	= 	srcs
OBJS_DIR 	= 	objs
DOWNLOADS	=	downloads

FLAGS		=	-Wall -Wextra -Werror -std=c++98 -g

SRCS		=	others.cpp \
				main.cpp \
				ServerConfig.cpp \
				ServerConfigParser.cpp \
				Routes.cpp \
				Response.cpp \
				RequestData.cpp \
				Server.cpp \
				ServersMonitor.cpp \
				CGIHandler.cpp \
				ErrorPage.cpp \
				ClientHandler.cpp

OBJS		=	$(addprefix $(OBJS_DIR)/,$(SRCS:.cpp=.o))

INC_PATH	=	-I./incs

COMPILE		=	$(CC) $(FLAGS) $(INC_PATH)

all:		$(NAME)
			mkdir -p $(DOWNLOADS)

$(NAME):	${OBJS}
			$(COMPILE) $(OBJS) -o $(NAME)

$(OBJS_DIR)/%.o: $(SRCS_DIR)/%.cpp
	mkdir -p $(OBJS_DIR)
	$(CC)   -D PRINT=1 $(FLAGS) $(INC_PATH) -o $@ -c $<

clean:
			rm -rf ${OBJS_DIR}
			rm -rf ${DOWNLOADS}/*
			rm -rf a.out

fclean:		clean
			rm -f ${NAME}

re:			fclean all

.PHONY:		clean fclean all re
