# Webserv

42 project to write a HTTP server

## How to test it :basketball:

```sh
make
./webserv configFile
```
ping http://localhost:18001
OR
```
curl -X GET -H "Server: servame" -H "Content-Type: text/plain" localhost:18001
curl -X POST http://localhost:18001 -F imagedata=@./test/pigeons.jpg
curl -X POST -H "Transfer-Encoding: chunked" http://localhost:18001 -F imagedata=@./test/pigeons.jpg
curl -X POST  -H "Transfer-Encoding: chunked" http://localhost:18001/cgi-bin/post_form_crop.php -F text1="hello1" -F text2="hello2" -F file1=@./test/testFile.txt
```

## Ressources :atom:

### Official docs
- [World Wide Web Consortium requirements for HTTP/1.1](https://www.w3.org/Protocols/rfc2616/rfc2616-sec1.html)
- [Hypertext Transfer Protocol -- HTTP/1.1](https://datatracker.ietf.org/doc/html/rfc2068)
- [HTTP Requests Method's definitions](https://datatracker.ietf.org/doc/html/rfc7231#section-4.3)

### Tutorials
- [Webserver in C (video)](https://www.youtube.com/watch?v=esXw4bdaZkc&list=PL9IEJIKnBJjH_zM5LnovnoaKlXML5qh17&index=2)
- [Another tutorial on how to make a server](https://medium.com/from-the-scratch/http-server-what-do-you-need-to-know-to-build-a-simple-http-server-from-scratch-d1ef8945e4fa)
- [Example of non-blocking I/O using select()](https://www.ibm.com/docs/en/i/7.3?topic=designs-example-nonblocking-io-select)

### Others
- [select vs poll](https://nima101.github.io/io_multiplexing)
- [Architectural Design](http://www.vishalchovatiya.com/what-is-design-pattern/)
- [Very useful tool to check HTTP requests and responses](https://reqbin.com)
- Connection management: [Persistent Connection](https://www.oreilly.com/library/view/http-the-definitive/1565925092/ch04s05.html) vs. [HTTP pipelining](https://developer.mozilla.org/en-US/docs/Web/HTTP/Connection_management_in_HTTP_1.x)

### CGI
- [Apache and CGI](https://httpd.apache.org/docs/2.4/howto/cgi.html)
- [RFC](https://datatracker.ietf.org/doc/html/rfc3875)


## Errors :wrench:
- Errors EAGAIN/EWOULDBLOCK returned by recv() mean there's no data to read at the moment meanwhile it means the [buffer is full (because the receiver hasn't read all the data yet) when returned by sent()](https://www.ibm.com/support/pages/why-does-send-return-eagain-ewouldblock) :white_check_mark:
- What is the correct way to send data over a socket: [let client know all the data has been sent](https://stackoverflow.com/questions/57740245/what-is-the-correct-way-to-use-send-on-sockets-when-the-full-message-has-not-b/57741537#57741537), [another idea](https://stackoverflow.com/questions/42097108/tcp-client-server-when-stop-read-socket) ... :white_check_mark:


## Todolist :dart:

### Response header's status code and message

As listed [here](https://en.wikipedia.org/wiki/List_of_HTTP_status_codes#cite_ref-10)
- **200 OK**
Standard response for successful HTTP requests. The actual response will depend on the request method used. In a GET request, the response will contain an entity corresponding to the requested resource. In a POST request, the response will contain an entity describing or containing the result of the action

- **204 No content**
The server successfully processed the request, and is not returning any content.

- **301 Moved Permanently**
This and all future requests should be directed to the given URI.

- **400 Bad Request**
The server cannot or will not process the request due to an apparent client error (e.g., malformed request syntax, size too large, invalid request message framing, or deceptive request routing).

- **403 Forbidden**
The request contained valid data and was understood by the server, but the server is refusing action. This may be due to the user not having the necessary permissions for a resource or needing an account of some sort, or attempting a prohibited action (e.g. creating a duplicate record where only one is allowed). This code is also typically used if the request provided authentication by answering the WWW-Authenticate header field challenge, but the server did not accept that authentication. The request should not be repeated.

- **404 Not Found**
The requested resource could not be found but may be available in the future. Subsequent requests by the client are permissible.

- **405 Method Not Allowed**
A request method is not supported for the requested resource; for example, a GET request on a form that requires data to be presented via POST, or a PUT request on a read-only resource.

- **408 Request Timeout**
The server timed out waiting for the request. According to HTTP specifications: "The client did not produce a request within the time that the server was prepared to wait. The client MAY repeat the request without modifications at any later time."

- **413 Payload Too Large** (RFC 7231)
The URI provided was too long for the server to process. Often the result of too much data being encoded as a query-string of a GET request, in which case it should be converted to a POST request.Called "Request-URI Too Long" previously.

- **414 URI Too Long** (RFC 7231)
The URI provided was too long for the server to process. Often the result of too much data being encoded as a query-string of a GET request, in which case it should be converted to a POST request.Called "Request-URI Too Long" previously.



### Content-Type

A quite thorough list [here](https://www.iana.org/assignments/media-types/media-types.xhtml#image)


### POST

 - [Returning Values from Forms: multipart/form-data (RFC2388)](https://www.rfc-editor.org/rfc/rfc7578)
 - [Generate examples](https://stackoverflow.com/questions/8659808/how-does-http-file-upload-work)
 - [POST method with curl explained](https://www.microfocus.com/documentation/idol/IDOL_12_0/MediaServer/Guides/html/English/Content/Shared_Admin/_ADM_POST_requests.htm)
