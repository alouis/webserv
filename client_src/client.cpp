/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   client.cpp                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: clucien <clucien@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/11/10 18:32:25 by clucien           #+#    #+#             */
/*   Updated: 2021/12/03 16:05:22 by clucien          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <iostream>
#include <string>
#include <map>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <unistd.h>

#define CPRINTL( str ) std::cerr << str << std::endl; // debug printing
#define cstring_map std::map< char, std::string >

int				retMsg( int retVal, std::string msg )
{
	std::cout << msg;
	return retVal;
}

cstring_map		mapArgs( int ac, char **av )
{
	cstring_map	args;

	for ( int i = 1; i < ac; ++i )
	{
		std::string	arg( av[ i ] );
		// --value
		if ( !arg.find( "--" ) )
		{
			if ( arg == "--chunked" )
				args[ 'c' ] = "true";
			if ( arg == "--verbose" )
				args[ 'v' ] = "true";
		}
		// -key val
		else if ( !arg.find( "-" )
				&& i + 1 < ac && av[ 1 ] )
		{
			args[ av[ i ][ 1 ] ] = av[ i + 1 ];
			++i;
		}
	}
	return args;
}

int				testSyntax( cstring_map args )
{
	if ( args.find( 'p' ) == args.end() )
		return 0;
	return 1;
}

std::string		fillRequest( cstring_map args )
{
	std::string	req;
	// headers
	req.append(
"Transfer-Encoding: chunked\nUser-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.14; rv:94.0) Gecko/20100101 Firefox/94.0\nContent-Length: 3\nAccept: image/avif,image/webp,*/*\nAccept-Language: en-US,en;q=0.5\nAccept-Encoding: gzip, deflate\nDNT: 1\nConnection: keep-alive\nReferer: http://localhost:18000/\nSec-Fetch-Dest: image\nSec-Fetch-Mode: no-cors\nSec-Fetch-Site: same-origin\n"
	);
	req.append( "\n" );
	// message_body
	if ( args.find( 'b' ) != args.end() )
		req.append( args[ 'b' ] );
	else
		req.append( "5\nHello\n" );
	return req;
}

std::string		createRequest( cstring_map args )
{
	std::string	request;

	// method
	request.append( "POST " );
	// uri
	if ( args.find( 'u' ) != args.end() )
		request.append( args[ 'u' ] + " " );
	else
		request.append( "/ " );
	request.append( "HTTP/1.1\n" );

	request.append( fillRequest( args ) );
	return request;
}

int				sendRequest( std::string request, cstring_map args )
{
	int	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if ( sockfd < 0 )
		return retMsg( 0, "error: failed to open socket\n" );

	std::string	serverName = "localhost";
	if ( args.find( 'h' ) != args.end()  )
		serverName = args[ 'h' ];

	struct hostent	*server = gethostbyname( serverName.c_str() );
	if ( server == NULL )
		return retMsg( 0, "error: can't find host\n");
	
	struct sockaddr_in	serv_addr;
	bzero( ( char * )&serv_addr, sizeof( serv_addr ) );
	serv_addr.sin_family = AF_INET;
	bcopy( ( char * )server->h_addr, 
			( char * )&serv_addr.sin_addr.s_addr,
			server->h_length );
	serv_addr.sin_port = htons( std::stoi( args[ 'p' ] ) );

	if ( connect( sockfd, ( struct sockaddr * ) &serv_addr, sizeof( serv_addr ) ) < 0 )
		return retMsg( 0, "error: unable to connect to server\n" );

	if ( args.find( 'v' ) != args.end() )
	{
		std::cout << "== SENT ==\n";
		std::cout << request;
	}
	if ( write( sockfd, request.c_str(), request.length() ) < 0 )
		return retMsg( 0, "error: unable to write to server\n" );

	char	buffer[ 256 ];
	if ( read(sockfd, buffer, 255) < 0 )
		return retMsg( 0, "error: unable to read from server\n" );
	
	std::cout << "== RECEIVED ==\n";
	std::cout << buffer << std::endl;

	close( sockfd );
	return 1;
}

int				main( int ac, char **av )
{
	cstring_map	args = mapArgs( ac, av );
	// for ( cstring_map::iterator begin = args.begin(); begin != args.end(); ++begin ) // prints args
		// CPRINTL( "'" << begin->first << "': '" << begin->second << "'"  )
	if ( !testSyntax( args ) )
	{
		std::cout << "error: bad syntax\n$> ./" << av[ 0 ] << " -p local_port -m method (--chunked --verbose -b message_body -u uri -h custom_host)\n";
		std::cout << "\nclucien's note: please be kind with syntax, error mgmt hasn't been quite handled for now ;) Lotta love <3\n";
		return 0;
	}
	std::string 	request = createRequest( args );
	if ( !sendRequest( request, args ) )
		return 0;

	if ( !sendRequest( "1\n \n", args ) )
		return 0;
	if ( !sendRequest( "6\nWolrd!\n", args ) )
		return 0;
	if ( !sendRequest( "0\n\n", args ) )
		return 0;
	return 1;
}
